<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, CONCAT(NOMBRES,' ',APELLIDOS)NOMBRES
								FROM tb_contacto");

$area = mysqli_query($conn,"SELECT ID_AREA, DESCRIPCION
								FROM tb_area");

$busqueda = mysqli_query($conn, "SELECT A.CAUSA, A.ID_CONTACTO,  CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, A.JUZGADO, A.ID_CASO, A.DESCRIPCION,
										DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), A.OBSERVACIONES, A.NUMERO_MP, 
										FISCALIA, JUZGADO, JUEZ, OFICIAL, DIRECCION_JUZGADO, ZONA_JUZGADO, TELEFONO_JUZGADO, A.CASO_ORIGEN
									FROM 	tb_caso A,
											tb_contacto B,
											tb_acceso C
									WHERE A.ID_CONTACTO = B.ID_CONTACTO
									  AND A.ID_CASO 	= C.ID_CASO
									  AND C.ID_USUARIO  = '".$nombre."'");

$sql = mysqli_query($conn, "SELECT NOMBRES, APELLIDOS, CUI, TELEFONO, DIRECCION, ZONA, EMAIL, ID_CONTACTO
							FROM tb_contacto");

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		ECHO $resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}									
										 
?>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

	<div class="wrapper-return">
		<button type="button" class="boton4"><a href="menu.php?id=22">Regresar</a></button>
		<button type="button" class="boton4"><a href="menu.php?id=8">Ir a Calendario</a></button>
	</div>


    <div class=" bajar">
        <div class="row">
			<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line"></div></div>
				<div class="col-md-4 titulo-seccion bajar_espacio"><p>CASOS</p></div>
				<div class="col-md-4"><div class="line"></div></div>
			</div>

			<div class="col-md-12 table-responsive bajar">
				<table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
					<thead>
						<tr>
							<th class="centrar">CAUSA</th>
							<th class="centrar">CLIENTE</th>
							<th class="centrar">JUZGADO</th>
							<th class="centrar">DESCRIPCI&Oacute;N</th>
							<th class="centrar">EDITAR</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($res = mysqli_fetch_array($busqueda)){
							$detalle = "<img src='img/detalle.png' width='35px'>";
								echo "<tr>";
									$id_caso 		= $res[4];
									$id_cliente		= $res[1];
									$descripcion	= $res[5];
									$fec_inicio		= $res[6];
									$fec_final		= $res[7];
									$observaciones	= $res[8];
									$causa 			= $res[0];
									$num_mp 		= $res[9];
									$fiscalia 		= $res[10];
									$juzgado 		= $res[11];
									$juez			= $res[12];
									$oficial 		= $res[13];
									$direc_juzgado	= $res[14];
									$zona_juzgado 	= $res[15];
									$tel_juzgado 	= $res[16];
									$origen 		= $res[17];

									echo "<td>$res[0]</td>";
									echo "<td width='30%'>$res[2]</td>";
									echo "<td>$res[3]</td>";
									echo "<td>$res[5]</td>";
									echo "<td width='30%'><a href='#' data-toggle='modal' data-target='#myModal'
									data-id_caso 		= '$id_caso'
									data-id_cliente		= '$id_cliente'
									data-descripcion	= '$descripcion'
									data-fec_inicio		= '$fec_inicio'
									data-fec_final		= '$fec_final'
									data-observaciones	= '$observaciones'
									data-causa 			= '$causa'
									data-num_mp			= '$num_mp'
									data-fiscalia 		= '$fiscalia'
									data-juzgado 		= '$juzgado'
									data-juez			= '$juez'
									data-oficial 		= '$oficial'
									data-direc_juzgado 	= '$direc_juzgado'
									data-zona_juzgado   = '$zona_juzgado'
									data-tel_juzgado 	= '$tel_juzgado'
									data-origen			= '$origen'
									><img class='img-caso' src='img/edit.png'></a></td>";
								echo "</tr>";
							} 
						?>           
					</tbody>
				</table>

		<div>
			<button type="button" class="hover"><a href="reporte.php?tmp=A" target="_blank">Reporte PDF</a></button>
			<button type="button" class="hover"><a href="genera_rep_excel.php?tmp=A">Reporte Excel</a></button>
		</div>

			</div>

        </div>
    </div>


	


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">MODIFICACI&Oacute;N CASOS</h4>
        </div>
		
		<div class="modal-body">

		<form action="menu.php?id=34" method="post" enctype="multipart/form-data">
			<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab">GENERALES CASO</a></li>
						<li><a href="#tab2" data-toggle="tab">DATOS JUZGADOS</a></li>
					</ul>
					
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
					<div style='display: none;'>
						<input type="text" class='form-control centrar' name='id_contacto' readonly=''>
					</div>
					<div style='display: none;'>
						<input type="text" class='form-control centrar' name='id_caso' readonly=''>
					</div>										

						<div>
							<label>CAUSA</label>
							 <input type="text" class='form-control centrar' name='causa'>
						</div>
						<div>
							<label>CASO ORIGEN</label>
							<select name="origen" class="form-control">
								<option value="0">SIN CASO ORIGEN</option>
								<?php
									$sdl = mysqli_query($conn, "SELECT ID_CASO, CAUSA
																FROM tb_caso");
									while($res = mysqli_fetch_array($sdl)){
										echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
									}							
								?>
							</select>
						</div>
						<div>
							<label>DESCRIPCION</label>
							<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
						</div>
						<div>
							<label>FECHA INICIO</label>
							<input type="text" name="fec_inicio" class="form-control centrar" id="datepicker" placeholder="FECHA INICIO" required="">
						</div>
						<div>
							<label>FECHA FINAL</label>
							<input type="text" name="fec_final" class="form-control centrar" id="datepicker_1" placeholder="FECHA FINAL">
						</div>
						<div>
							<label>OBSERVACIONES</label>
							<input type="text" name="observaciones" class="form-control upper" placeholder="OBSERVACIONES">
						</div>

					</div>
					<div  class="tab-pane" id="tab2">
					<div>
					<label>N&Uacute;MERO MP</label>
					<input type="text" name="numero_mp" class="form-control upper" placeholder="N&uacute;mero MP">
				</div>
				<div>
					<label>FISCALIA</label>
					<input type="text" name="des_fiscalia" class="form-control upper" placeholder="Fiscalia">
				</div>
				<div>
					<label>JUZGADO</label>
					<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
				</div>
				<div>
					<label>JUEZ</label>
					<input type="text" name="juez" class="form-control upper" placeholder="Juez">
				</div>
				<div>
					<label>OFICIAL</label>
					<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
				</div>	
				<div>
					<label>DIRECCI&Oacute;N JUZGADO</label>
					<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
				</div>	
				<div>
					<label>ZONA JUZGADO</label>
					<select name="zona_juz" class="form-control">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>						
					</select>
				</div>
				<div>
					<label>TELEFONO JUZGADO</label>
					<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
				</div>		
					</div>
				</div>

			</div>	
			<div class="boton-formulario bajar">
				<button type="submit" class="boton3">GRABAR</button>
				<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
			</div>					
		</form>			

       </div>
		

		</div>	
		
   
    </div>
  </div>
  <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
		var id_caso 		= $(e.relatedTarget).data('id_caso');
		var id_cliente 		= $(e.relatedTarget).data('id_cliente');
		var id_area 		= $(e.relatedTarget).data('id_area');
		var descripcion		= $(e.relatedTarget).data('descripcion');
		var fec_inicio		= $(e.relatedTarget).data('fec_inicio');
		var fec_final		= $(e.relatedTarget).data('fec_final');
		var observaciones	= $(e.relatedTarget).data('observaciones');
		var causa			= $(e.relatedTarget).data('causa');
		var num_mp			= $(e.relatedTarget).data('num_mp');
		var fiscalia    	= $(e.relatedTarget).data('fiscalia');
		var juzgado	    	= $(e.relatedTarget).data('juzgado');
		var juez	    	= $(e.relatedTarget).data('juez');
		var oficial	    	= $(e.relatedTarget).data('oficial');
		var direccion    	= $(e.relatedTarget).data('direc_juzgado');
		var zona	    	= $(e.relatedTarget).data('zona_juzgado');
		var tel_juzgado    	= $(e.relatedTarget).data('tel_juzgado');
		var origen 	    	= $(e.relatedTarget).data('origen');


		$(e.currentTarget).find('input[name="id_caso"]').val(id_caso);
		$(e.currentTarget).find('input[name="id_contacto"]').val(id_cliente);
		$(e.currentTarget).find('input[name="id_area"]').val(id_area);
		$(e.currentTarget).find('input[name="descripcion"]').val(descripcion);
		$(e.currentTarget).find('input[name="fec_inicio"]').val(fec_inicio);
		$(e.currentTarget).find('input[name="fec_final"]').val(fec_final);
		$(e.currentTarget).find('input[name="observaciones"]').val(observaciones);
		$(e.currentTarget).find('input[name="causa"]').val(causa);
		$(e.currentTarget).find('input[name="numero_mp"]').val(num_mp);
		$(e.currentTarget).find('input[name="des_fiscalia"]').val(fiscalia);
		$(e.currentTarget).find('input[name="juzgado"]').val(juzgado);
		$(e.currentTarget).find('input[name="juez"]').val(juez);
		$(e.currentTarget).find('input[name="oficial"]').val(oficial);
		$(e.currentTarget).find('input[name="direccion_juzgado"]').val(direccion);
		$(e.currentTarget).find('select[name="zona_juz"]').val(zona);
		$(e.currentTarget).find('input[name="tel_juzgado"]').val(tel_juzgado);
		$(e.currentTarget).find('select[name="origen"]').val(origen);

	});
  </script>
    	<div class=" bajar">
		<div class="row">
			<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
				<div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>NUEVOS CASOS</p></div>
				<div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			</div> 

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
			<center>
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">CREAR NUEVO CASO</button>
			</center>
			</div>			
		</div> 			

		</div>
	</div>

	<div class="">
	<div class="modal fade" id="ModalCrea" role="dialog">
		<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CREACI&Oacute;N DE CASOS</h4>
			</div>
			<div class="modal-body cuerpo">

			<form action="menu.php?id=30" method="post" enctype="multipart/form-data" onsubmit="return validar();">
			
			

			<div class="tabbable1">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab">GENERALES CASO</a></li>
					<li><a href="#tab2" data-toggle="tab">DATOS JUZGADOS</a></li>
				</ul>
        <div class="tab-content">
			<div class="tab-pane active" id="tab1">
			<div>
					<label>CONTACTO</label>
						<select name="id_contacto" class="form-control" autofocus="">
							<option value="">SELECCIONAR CONTACTO</option>
						<?php
						while ($row = mysqli_fetch_array($contacto))
						{
							echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['NOMBRES'] . '</option>' . "\n";
						}
						?>
						</select>
					</div>
					<div>
					<?php

					echo "<label for=''>RUTA DE CASO</label>";
									echo "<select name='ruta' id='ruta' class='form-control upper'>";

					function listar_archivos($carpeta){
						if(is_dir($carpeta)){
							if($dir = opendir($carpeta)){
								while(($archivo = readdir($dir)) !== false){
									if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
										$archivo = $archivo;
										echo "<option value='$carpeta$archivo'>$archivo</option>";
									}
								}
								closedir($dir);
							}
						}
						}
					
						echo listar_archivos(__DIR__."/CASOS/");


							
						echo "</select>";
				?>

					</div>

					<div>
						<label>CASO ORIGEN</label>
						<select name="origen" class="form-control">
							<option value="0">SIN CASO ORIGEN</option>
							<?php
								$sdl = mysqli_query($conn, "SELECT ID_CASO, CAUSA
															FROM tb_caso");
								while($res = mysqli_fetch_array($sdl)){
									echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
								}							
							?>
						</select>
					</div>
					
					<div>
						<label>DESCRIPCION</label>
						<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
					</div>
					<div>
						<label>FECHA INICIO</label>
						<input type="text" name="fec_inicio" class="form-control centrar" id="datepicker" placeholder="FECHA INICIO" required="">
					</div>
					<div>
						<label>FECHA FINAL</label>
						<input type="text" name="fec_final" class="form-control centrar" id="datepicker_1" placeholder="FECHA FINAL">
					</div>
					<div>
						<label>OBSERVACIONES</label>
						<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
					</div>

					<div>
						<label for="">ADJUNTAR ARCHIVO</label>
						<input type="file" name="file" class="form-control">
					</div>


			</div>
			<div class="tab-pane" id="tab2">
				<div>
					<label>CAUSA</label>
					<input type="text" name="causa" class="form-control upper" placeholder="Causa de Caso" id="causa">
				</div>
				<div>
					<label>N&Uacute;MERO MP</label>
					<input type="text" name="mun_mp" class="form-control upper" placeholder="N&uacute;mero MP">
				</div>
				<div>
					<label>FISCALIA</label>
					<input type="text" name="fiscalia" class="form-control upper" placeholder="Fiscalia">
				</div>
				<div>
					<label>JUZGADO</label>
					<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
				</div>
				<div>
					<label>JUEZ</label>
					<input type="text" name="juez" class="form-control upper" placeholder="Juez">
				</div>
				<div>
					<label>OFICIAL</label>
					<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
				</div>	
				<div>
					<label>DIRECCI&Oacute;N JUZGADO</label>
					<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
				</div>	
				<div>
					<label>ZONA JUZGADO</label>
					<select name="zona_juz" class="form-control">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>						
					</select>
				</div>
				<div>
					<label>TELEFONO JUZGADO</label>
					<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
				</div>															
			</div>
        </div>
			<div class="boton-formulario bajar">
				<button type="submit" class="boton3">GRABAR</button>
				<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
			</div>
		</form>
        </div>
			</div>
		</div>
		
		</div>
	</div>
</div>