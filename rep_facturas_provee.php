<?php
require_once 'db/conexion.php';

$sql = mysqli_query($conn, "SELECT TRIM(CONCAT_WS(' ', a.NOMBRE_PROVEEDOR, a.APELLIDO_PROVEEDOR , a.NOMBRE_EMPRESA))NOMBRE, b.SERIE, b.FACTURA, b.FECHA_EMISION, b.OBSERVACIONES, b.TOTAL
                            FROM tb_proveedor a,
                                tb_factura_proveedor b
                            WHERE a.ID_PROVEEDOR = b.ID_PROVEEDOR");

?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: -30px !important;"><p>CONSULTA FACTURA PROVEEDORES</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
	        <thead>
	            <tr>
					<th class="centrar">NOMBRE</th>
	                <th class="centrar">SERIE</th>
	                <th class="centrar">FACTURA</th>
	                <th class="centrar">FECHA EMISI&Oacute;N</th>
					<th class="centrar">OBSERVACIONES</th>
					<th class="centrar">MONTO   </th>
	            </tr>
	        </thead>
	        <tbody>
	       	<?php
			while ($row = mysqli_fetch_array($sql)){
				echo "<tr>";
                    echo "<td>";
                        echo $row[0];
                    echo "</td>";
                    echo "<td>";
                        echo $row[1];
                    echo "</td>";   
                    echo "<td>";
                        echo $row[2];
                    echo "</td>";
                    echo "<td>";
                        echo $row[3];
                    echo "</td>";
                    echo "<td>";
                        echo $row[4];
                    echo "</td>";
                    echo "<td>";
                        echo $row[5];
                    echo "</td>";                                                                                                 
				echo "</tr>";
				} 
			?>           
	        </tbody>
	    </table>

    </div>