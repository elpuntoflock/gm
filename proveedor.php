<?php
require_once 'db/conexion.php';

$sql = mysqli_query($conn, "SELECT TRIM(CONCAT(NOMBRE_EMPRESA,' ',NOMBRE_PROVEEDOR,' ',APELLIDO_PROVEEDOR))NOMBRE, NOMBRE_CONTACTO,
                                    CARGO_CONTACTO, TELEFONO_CONTACTO, CELULAR_CONTACTO, 
                                    CASE WHEN TIPO = 'P' THEN 'PROVEEDOR'
                                         WHEN TIPO = 'C' THEN 'COLABORADOR'
                                    ELSE TIPO END AS TIPOS,
                                    CASE WHEN STATUS = 'A' THEN 'ACTIVO'
                                         WHEN STATUS = 'B' THEN 'BAJA'
                                         WHEN STATUS = 'I' THEN 'INACTIVO'
                                    ELSE STATUS END AS ESTADO
                             FROM tb_proveedor");
?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: 5px !important;"><p>PROVEEDORES</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th>NOMBRE PROVEEDOR</th>
                    <th>NOMBRE CONTACTO</th>
                    <th>CARGO CONTACTO</th>
                    <th>TELEFONO</th>
                    <th>CELULAR</th>
                    <th>TIPO</th>                    
                    <th>ESTATUS</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while ($row = mysqli_fetch_array($sql)){
                    echo "<tr>";
                        echo "<td>";
                            echo $row['NOMBRE'];
                        echo "</td>";
                        echo "<td>";
                            echo $row['NOMBRE_CONTACTO'];
                        echo "</td>";     
                        echo "<td>";
                            echo $row['CARGO_CONTACTO'];
                        echo "</td>";
                        echo "<td>";
                            echo $row['TELEFONO_CONTACTO'];
                        echo "</td>";  
                        echo "<td>";
                            echo $row['CELULAR_CONTACTO'];
                        echo "</td>"; 
                        echo "<td>";
                            echo $row['TIPOS'];
                        echo "</td>";
                        echo "<td>";
                            echo $row['ESTADO'];
                        echo "</td>";                                                                                                                                                
                    echo "</tr>";
                }
            ?>
            </tbody>
        </table>
</div>


	<div class=" bajar">
		<div class="row">
			<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
				<div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>NUEVOS INGRESOS</p></div>
				<div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			</div> 

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">NUEVO PROVEEDOR</button>
			</div>			
		</div>

<div id="ModalCrea" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
        <h4 class="modal-title">INGRESO DE PROVEEDORES</h4>
      </div>
      <div class="modal-body">
        <form action="menu.php?id=48" method="post">

<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab">DATOS GENERALES</a></li>
					<li><a href="#tab2" data-toggle="tab">DATOS COMPLEMENTO</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
					
                    <div>
                        <label for="">NOMBRE EMPRESA</label>
                        <input type="text" name="empresa" class="form-control upper" placeholder="Nombre Empresa">
                    </div>
                    <div>
                        <label for="">NOMBRES PROVEEDOR</label>
                        <input type="text" name="nombre_proveedor" class="form-control upper" placeholder="Nombres Proveedor">
                    </div>
                    <div>
                        <label for="">APELLIDOS PROVEEDOR</label>
                        <input type="text" name="apellido_proveedor" class="form-control upper" placeholder="Apellidos Proveedor">
                    </div> 
                    <div>
                        <label for="">DIRECCI&Oacute;N</label>
                        <input type="text" name="direccion" class="form-control upper" placeholder="Direcci&oacute;n">
                    </div>
                    <div>
                        <label for="">ZONA</label>
                        <select name="zona" class="form-control upper">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="21">21</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                        </select>                        
                    </div>
                    <div>
                        <label for="">NIT</label>
                        <input type="text" name="nit" class="form-control upper" placeholder="Nit">
                    </div>
                    <div>
                        <label for="">CUI</label>
                        <input type="text" name="cui" class="form-control upper" placeholder="Cui">
                    </div> 

                </div>

					<div class="tab-pane" id="tab2">
                        <div>
                            <label for="">NOMBRES CONTACTO</label>
                            <input type="text" name="n_contacto" class="form-control upper" placeholder="Nombres Contacto">
                        </div>
                        <div>
                            <label for="">APELLIDOS CONTACTO</label>
                            <input type="text" name="ap_contacto" class="form-control upper" placeholder="Apellidos Contacto">
                        </div>
                        <div>
                            <label for="">CARGO CONTACTO</label>
                            <input type="text" name="cargo" class="form-control upper" placeholder="Cargo Contacto">
                        </div>
                        <div>
                            <label for="">TEL&Eacute;FONO CONTACTO</label>
                            <input type="text" name="telefono" class="form-control upper" placeholder="tel&eacute;fono contacto">
                        </div>
                        <div>
                            <label for="">CELULAR CONTACTO</label>
                            <input type="text" name="celular" class="form-control upper" placeholder="celular contacto">
                        </div> 
                        <div>
                            <label for="">TIPO</label>
                            <select name="tipo" class="form-control">
                                <option value="C">COLABORADOR</option>
                                <option value="P">PROVEEDOR</option>
                            </select>
                        </div> 
                        <div class="boton-formulario bajar">
							<button type="submit" class="boton3">GRABAR</button>
							<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
					</div>                                              
					</div>

				</div>


			</div>


        </form>
      </div>

    </div>

  </div>
</div>         	