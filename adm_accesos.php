
<?php
include('seguridad.php');
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT ID_CASO, DESCRIPCION, CAUSA
                            FROM tb_caso");

$usuario = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ', NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE
								FROM tb_usuario");

$usuarioII = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ',NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE
                                FROM tb_usuario");	
                                
$sql_user = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ',NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE, EMAIL,
                                        NOMBRES, NOMBRE2, APELLIDO1, APELLIDO2, CELULAR, TIPO_USUARIO
                                FROM tb_usuario");                                

?>

<style>
.centrar {
    text-align: center !important;
}
.upper {
    text-transform: uppercase !important;
}
.color {
    color: #fff;
}
</style>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="">
	<div class="row">
        <div class="top-line">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>PANEL DE ADMINISTRACI&Oacute;N</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>	
		


	</div>
</div>

<!--**********************************************************-->

<!--**********************************************************-->
<div class="wrapper-dashboard" style='margin-top: 70px;'>
        <div class="row m-t-25" style="margin-top: -55px;">
            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span>Accesos a Casos usuario</span>
                                <span><a href="" data-toggle="modal" data-target="#exampleModal"><h2><i class="fas fa-key"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span>Accesos Men&uacute; a usuario</span>
                                <span><a href=""  data-toggle="modal" data-target="#exampleModalItem"><h2><i class="fas fa-sitemap"></i></h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span>creaci&oacute;n usuarios</span>
                                <span><a href="" data-toggle="modal" data-target="#ModalUser"><h2><i class="fas fa-user-plus"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

        </div>

</div>
<!--*****************************************************************************************-->
<div class="wrapper-dashboard" >
        <div class="row m-t-25" style="margin-top: -55px;">
            <div class="col-md-2" style='margin-top: 45px;'></div>
            <div class="col-md-4 bajar">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span>Eliminar Item por Usuario</span>
                                <span><a href="" data-toggle="modal" data-target="#TableUserItem"><h2><i class="fas fa-users-cog"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 bajar">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span>Eliminar Casos por Usuario</span>
                                <span><a href="" data-toggle="modal" data-target="#myModalD"><h2><i class="fa fa-trash"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">

            <!-- Modal -->
            <div class="modal fade" id="myModalD" role="dialog">
                <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">ELIMINAR PERMISOS DE CASOS</h4>
                    </div>
                    <div class="modal-body">
                    <form action="edita_usuario.php" method="post">

                            <div class="col-md-12 table-responsive bajar">
                                    <table id="exampleTable1" class="display nowrap table table-striped table-bordered" style="width:100%;">
                                    <thead style='background-color: #005691;'>
                                            <tr>
                                                <th class="centrar color">USUARIO</th>
                                                <th class="centrar color">DESCRIPCI&Oacute;N  DE CASO</th>
                                                <th class="centrar color">---</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql1 = mysqli_query($conn, "SELECT b.ID_USUARIO, b.ID_CASO, a.CAUSA
                                                                        FROM tb_caso a,
                                                                            tb_acceso b
                                                                        WHERE a.ID_CASO = b.ID_CASO");

                                        while ($res1 = mysqli_fetch_array($sql1)){
                                                echo "<tr class= 'wrapper-text'>";
                                                    $user	= $res1[0];
                                                    $img = "<img src='img/eliminar.png' width='25px'>";
                                                    $caso = $res1[1];
                                                    $detalle = $res1[2];
                                                    
                                                    echo "<td>$user</td>";
                                                    echo "<td>$detalle</td>";
                                                    echo "<td><a href='menu.php?id=37&tmp=$caso&var=$user'>$img</a></td>";
                                                echo "</tr>";
                                            }                                                     
                                            ?>
                                        </tbody>
                                    </table>
                            </div>		

<div class="modal-footer centrar bajar">
    <!--button type="submit" class="boton3">GRABAR</button-->
    <button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
</div>	
</form>
                    </div>
                </div>
                
                </div>
            </div>
            
            </div>

        </div>

</div>
<!--/***************************************************************************************/-->

<div class="wrapper-dashboard" >
    <div class="row m-t-25" style="margin-top: -55px;">

    <div class="">
	<div class="row bajar">
        <div class="top-line">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>CONTROL USUARIOS</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>	
		


	</div>
</div>

			<div class="col-md-12 table-responsive bajar">
				<table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
					<thead>
						<tr>
							<th class="centrar">USUARIO</th>
							<th class="centrar">NOMBRES</th>
							<th class="centrar">EMAIL</th>
                            <th class="centrar">ELIMINAR</th>
                            <th class="centrar">EDITAR</th>
						</tr>
					</thead>
					<tbody>
						<?php
        				while ($fond = mysqli_fetch_array($sql_user)){
                                $d_usuario  = $fond[0];
                                $p_nombre   = $fond[3];
                                $s_nombre   = $fond[4];
                                $p_apellido = $fond[5];
                                $s_apellido = $fond[6];
                                $email      = $fond[2];
                                $celular    = $fond[7];
                                $tipo_user  = $fond[8];
								echo "<tr>";
									echo "<td style='font-weight: bold;'>$fond[0]</td>";
									echo "<td style='text-align: left;'>$fond[1]</td>";
                                    echo "<td style='text-align: left;'>$fond[2]</td>";
                                    echo "<td width='30%'><a href='menu.php?id=32&tmp=$fond[0]' ><img class='img-caso' src='img/eliminar.png'></a></td>";
                                    echo "<td width='30%'><a href='#' data-toggle='modal' data-target='#myModal'
                                    data-usuario    = '$d_usuario'
                                    data-nombre1    = '$p_nombre'
                                    data-nombre2    = '$s_nombre'
                                    data-apellido1  = '$p_apellido'
                                    data-apellido2  = '$s_apellido'
                                    data-email      = '$email'
                                    data-celular    = '$celular'
                                    data-tipo       = '$tipo_user'
                                    ><img class='img-caso' src='img/edit.png'></a></td>";
								echo "</tr>";
							} 
						?>           
					</tbody>
				</table>

		<div>

    </div>
</div>

<!--**************************** MODAL ACCESO A LOS CASOS **************************-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ACCESO POR CASO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	  <form action="menu.php?id=18" method="post">

		<div>
			<label>SELECCIONE CASO</label>
			<select name="caso" class="form-control" required="">
				<?php
				while ($row = mysqli_fetch_array($sql))
				{
					$causatxt =  empty($row['CAUSA']) ? ' sin causa' : $row['CAUSA'];
					echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '-' . $causatxt   . '</option>' . "\n";
				}
				?> 						
			</select>
		</div>

		<div>
			<label>SELECCIONE USUARIO</label>
			<select name="usuario" class="form-control" required="">
				<?php
				while ($rowX = mysqli_fetch_array($usuario))
				{
				echo '<option value="' . $rowX['ID_USUARIO']. '">' . $rowX['NOMBRE'] . '</option>' . "\n";
				}
				?> 						
			</select>
		</div>

	<div style="margin-top: 40px;">
		<div class="boton-formulario">
			  <button type="submit" class="boton3">ACCESO</button>
		</div>
	</div>

</div>
</form>
      </div>
    </div>
  </div>
</div>

<!--****************************MODAL ACCESO A MENU**************************-->

<div class="modal fade" id="exampleModalItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ACCESO A MEN&Uacute;</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	  <form action="menu.php?id=28" method="post">

		<div>
			<label>SELECCIONE MEN&Uacute;</label>
			<select name="item" class="form-control" required="">
				<option value="">SELECCIONAR ITEM</option>
				<option value="10">CARPETAS</option>
				<option value="1">CONTACTOS</option>
				<option value="2">CASOS</option>
				<option value="16">TAREAS</option>
				<option value="8">CALENDARIO</option>
				<option value="9">CONSULTAS</option>
				<option value="4">CTA. CORRIENTE</option>
				<option value="41">BUSCADOR</option>
                <option value="43">FACTURACI&Oacute;N</option>
                <option value="45">EDITAR FACTURACI&Oacute;N</option>
			</select>
		</div>

		<div>
			<label>SELECCIONE USUARIO</label>
			<select name="usuario" class="form-control" required="">
				<?php
				while ($rowy = mysqli_fetch_array($usuarioII))
				{
				echo '<option value="' . $rowy['ID_USUARIO']. '">' . $rowy['NOMBRE'] . '</option>' . "\n";
				}
				?> 						
			</select>
		</div>

	<div style="margin-top: 40px;">
		<div class="boton-formulario">
			  <button type="submit" class="boton3">ACCESO</button>
		</div>
	</div>

</div>

</form>

      </div>
    </div>
  </div>
</div>

<!--****************************MODAL CREACION DE USUARIOS **************************-->

<div class="modal fade" id="ModalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">CREACI&Oacute;N DE USUARIOS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	    <form action="menu.php?id=29" method="post" onsubmit="return validar();">
            <div>
                <label>DEFINIR USUARIO</label>
                <input type="text" name="id_usuario" class="form-control centrar upper" placeholder="INGRESAR EL USUARIO" id="usuario" autofocus="">
            </div>

            <div class="bajar">
                <label>PRIMER NOMBRE</label>
                <input type="text" name="p_nombre" class="form-control upper" placeholder="Primer Nombre" id="pnombre">
            </div>
            <div>
                <label>SEGUNDO NOMBRE</label>
                <input type="text" name="s_nombre" class="form-control upper" placeholder="Segundo Nombre">
            </div>	
            <div>
                <label>PRIMER APELLIDO</label>
                <input type="text" name="p_apellido" class="form-control upper" placeholder="Primer Apellido" id="papellido">
            </div> 
            <div>
                <label>SEGUNDO APELLIDO</label>
                <input type="text" name="s_apellido" class="form-control upper" placeholder="Segundo Apellido">
            </div>    
            <div>
                <label>SEXO</label>
                <select name="sexo" class="form-control">
                    <option value="M">MASCULINO</option>
                    <option value="F">FEMENINO</option>
                </select>
            </div> 

            <div>
                <label>CELULAR</label>
                <input type="text" name="celular" class="form-control centrar" placeholder="Celular" id="celular">
            </div> 
            <div>
                <label>CORREO ELECTRONICO</label>
                <input type="text" name="email" class="form-control centrar" placeholder="Correo Electronico" id="email">
            </div>

            <div>
                <label>TIPO DE USUARIO</label>
                <select name="tipo" class="form-control">
                    <option value="1">ADMINISTRADOR</option>
                    <option value="2">OPERATIVO</option>
                </select>
            </div> 

            <div>
                <label>CONTRASE&Ntilde;A</label>
                <input type="password" name="password" class="form-control centrar" placeholder="Contrase&ntilde;a Generica" id="password" require>
            </div>                    	

            <div style="margin-top: 40px;">
                <div class="boton-formulario">
                    <button type="submit" class="boton3">GRABAR</button>
                </div>
            </div>

        </form>

      </div>
    </div>
  </div>
</div>

<script>
    function validar(){
        var usuario     = document.getElementById('usuario').value;
        var nombre      = document.getElementById('pnombre').value;
        var apellido    = document.getElementById('papellido').value;
        var celular     = document.getElementById('celular').value;
        var email       = document.getElementById('email').value;
        var contra      = document.getElementById('password').value;

    if(usuario == false){
        alert('El USUARIO es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }         

    if(nombre == false){
        alert('El campo Primer Nombre es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }  

    if(apellido == false){
        alert('El campo Primer Apellido es obligatorio FAVOR DE INGRESARLO....');
        return false;
    } 

    if(celular == false){
        alert('El campo Celular es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }  

    if(email == false){
        alert('El campo Correo Electronico es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }                   
        
    if(contra == false){
        alert('El campo contraseña es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }

    }
</script>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">MODIFICACI&Oacute;N USUARIO</h4>
        </div>
		
		<div class="modal-body">

		<form action="menu.php?id=33" method="post">
			<div>
				<label>USUARIO</label>
				<input type="text" name="d_usuario" id='d_usuario' class="form-control centrar" readonly=''>
			</div>

			<div>
				<label>PRIMER NOMBRE</label>
				<input type="text" name="p_nombre" id='p_nombre' class="form-control upper centrar" placeholder="PRIMER NOMBRE">
			</div>

			<div>
				<label>SEGUNDO NOMBRE</label>
				<input type="text" name="s_nombre" id='s_nombre' class="form-control upper centrar" placeholder="SEGUNDO NOMBRE">
			</div>

			<div>
	  			<label>PRIMER APELLIDO</label>
	  			<input type="text" name="p_apellido" id="p_apellido" class="form-control upper centrar" placeholder="PRIMER APELLIDO">
		  	</div>			

			<div>
	  			<label>SEGUNDO APELLIDO</label>
	  			<input type="text" name="s_apellido" id="s_apellido" class="form-control upper centrar" placeholder="SEGUNDO APELLIDO">
		  	</div>

			<div>
                <label>EMAIL</label>
                <input type="text" name="correo" id="correo" class="form-control centrar" placeholder="EMAIL">
			</div>

			<div>
                <label>CELULAR</label>
                <input type="text" name="cel" id="cel" class="form-control upper centrar" placeholder="CELULAR">
            </div>
            <div>
                <label>TIPO DE USUARIO</label>
                <select name="tipo_user" class="form-control">
                    <option value="1">ADMINISTRADOR</option>
                    <option value="2">OPERATIVO</option>
                </select>
            </div>             

       </div>
		
			<div class="modal-footer">
				<button type="submit" class="boton3">GRABAR</button>
				<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
			</div>	
		</form>
		</div>	
		
   
    </div>
</div>

    <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
        var d_usuario 	= $(e.relatedTarget).data('usuario');
        var p_nombre 	= $(e.relatedTarget).data('nombre1');
        var s_nombre 	= $(e.relatedTarget).data('nombre2');
        var apellido1 	= $(e.relatedTarget).data('apellido1');
        var apellido2 	= $(e.relatedTarget).data('apellido2');
        var correo  	= $(e.relatedTarget).data('email');
        var celular  	= $(e.relatedTarget).data('celular');
        var tipo      	= $(e.relatedTarget).data('tipo');

        $(e.currentTarget).find('input[name="d_usuario"]').val(d_usuario);
        $(e.currentTarget).find('input[name="p_nombre"]').val(p_nombre);
        $(e.currentTarget).find('input[name="s_nombre"]').val(s_nombre);
        $(e.currentTarget).find('input[name="p_apellido"]').val(apellido1);
        $(e.currentTarget).find('input[name="s_apellido"]').val(apellido2);        
        $(e.currentTarget).find('input[name="correo"]').val(correo);        
        $(e.currentTarget).find('input[name="cel"]').val(celular);
        $(e.currentTarget).find('select[name="tipo_user"]').val(tipo);


	});
  </script>

  <!--******************************* TABLA DE USUARIOS POR ITEM *********************************-->
  <style>
  #example1_filter {
      text-align: center !important;
      
  }

  #example1_filter > label {
    font-weight: bold !important;
  }

  .wrapper-text {
      font-weight: bold;
  }
  .dataTables_length {
        font-size: 16px;
        text-align: center;
  }

  .dataTables_length > label {
    font-weight: bold !important; 
  }

  .color {
      color: #fff;
  }

  </style>

<div class="modal fade" id="TableUserItem" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">CONTROL DE ITEM POR USUARIO</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
		
		<div class="modal-body">

		<form action="edita_usuario.php" method="post">

			<div class="col-md-12 table-responsive bajar">
				<table id="example1" class="display nowrap table table-striped table-bordered" style="width:100%;">
				<thead style='background-color: #005691;'>
						<tr>
							<th class="centrar color">USUARIO</th>
							<th class="centrar color">DESCRIPCI&Oacute;N  DE ITEM</th>
							<th class="centrar color">---</th>
						</tr>
					</thead>
					<tbody>
						<?php
                        $sql = mysqli_query($conn, "SELECT ID_USUARIO, ITEM
                                                    FROM tb_acceso_item");

                    while ($res = mysqli_fetch_array($sql)){
                            echo "<tr class= 'wrapper-text'>";
                                $id_usuario	= $res[0];
                                $img = "<img src='img/eliminar.png' width='25px'>";
                                $item = $res[1];
                                
                                if($item == 10){
                                    $descripcion = "CARPETAS";
                                }
                                if($item == 1){
                                    $descripcion = "CONTACTOS";
                                }
                                if($item == 2){
                                    $descripcion = "CASOS";
                                }
                                if($item == 16){
                                    $descripcion = "CALENDARIO";
                                }
                                if($item == 8){
                                    $descripcion = "CONSULTAS";
                                }
                                if($item == 9){
                                    $descripcion = "CTA. CORRIENTE";
                                }
                                if($item == 4){
                                    $descripcion = "ADMIN ACCESOS";
                                }

                                echo "<td>$id_usuario</td>";
                                echo "<td>$descripcion</td>";
                                echo "<td><a href='menu.php?id=35&tmp=$item&var=$id_usuario'>$img</a></td>";
                            echo "</tr>";
                        }                                                     
                        ?>
					</tbody>
				</table>
			</div>		

			<div class="modal-footer centrar bajar">
				<!--button type="submit" class="boton3">GRABAR</button-->
				<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
			</div>	
		</form>
		</div>	
		
   
    </div>
</div>


  <!--******************************* FINALIZA TABLA DE USUARIOS POR ITEM *********************************-->