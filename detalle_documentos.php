<?php
include('seguridad.php');
?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="wrapper-return">
	<button type="button" class="boton4"><a href="menu.php?id=2">Regresar</a></button>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion"><p>ARCHIVOS POR CASO</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>
        <?php
            $usuario 	= $_SESSION['usuario'];
            $usuario 	= strtoupper($usuario);

            require_once('db/conexion.php');

            $caso = $_REQUEST['ftc'];

            $sql = mysqli_query($conn, "SELECT a.id_document, a.descripcion, a.ruta
                                        FROM tb_documento a,
                                            tb_acceso b,
                                            tb_usuario c
                                        WHERE a.id_caso = b.ID_CASO
                                        AND b.ID_USUARIO = c.ID_USUARIO
                                        AND c.ID_USUARIO = '".$usuario."'
                                        AND a.id_caso = '".$caso."'");

        ?>

			<div class="col-md-12 table-responsive bajar">
				<table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
				<thead>
						<tr>
							<!--th class="centrar">ID DOCUMENTO</th-->
                            <th class="centrar">TIPO</th>
							<th class="centrar">DESCRIPCI&Oacute;N</th>
							<th class="centrar">DESCARGAR</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($res = mysqli_fetch_array($sql)){
                            
                            $id             = $res[0];
                            $descripcion    = strtoupper($res[1]);
                            $ruta           = $res[2];

                            $info = new SplFileInfo($descripcion);
                            $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                            if($extension == 'XLS' OR $extension == 'XLSX'){
                                $extension = "<img class='wrapper-imagen' src='img/document/excel.jpg' />";
                            }elseif($extension == 'PNG' OR $extension == 'JPG'){
                                $extension = "<img class='wrapper-imagen' src='img/document/imagenes.png' />";
                            }elseif($extension == 'SQL'){
                                $extension = "<img class='wrapper-imagen' src='img/document/sql.png' />";
                            }elseif($extension == 'PDF'){
                                $extension = "<img class='wrapper-imagen' src='img/document/pdf.png' />";
                            }elseif($extension == 'DOC' OR $extension == 'DOCX'){
                                $extension = "<img class='wrapper-imagen' src='img/document/word.png' />";
                            }elseif($extension == 'TXT'){
                                $extension = "<img class='wrapper-imagen' src='img/document/txt.png' />";
                            }

                            echo "<tr>";
                                //echo "<td style='text-align: center; with: 45px;'>$id</td>";
                                echo "<td style='text-align: center; with: 45px;'>$extension</td>";
                                echo "<td width='30%' style='text-align: left;'>$descripcion</td>";
                                echo "<td style='text-align: center;'><a href='$ruta' target='_blanck'> <i class='fa fa-download fa-1x' aria-hidden='true'></i></a></td>";
                            echo "</tr>";
                        } 
						?>           
					</tbody>
				</table>



			</div>