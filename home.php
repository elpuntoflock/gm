<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');
/*
$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }*/
    
    $casos = mysqli_query($conn, "SELECT COUNT(*)CUENTA
                                    FROM tb_acceso_item
                                    WHERE id_usuario = '".$nombre."'
                                      AND ITEM = 2");

    while($val_caso = $casos->fetch_array(MYSQLI_ASSOC)){

    $res_caso1 = $val_caso['CUENTA'];

    }    

?>
<br>
<br>

<link rel="stylesheet" type="text/css" href="assets/css/inicio.css">


        <div class="row">
            <div class="col-md-12">
                <div class="wrapper-logo-secondary">
                    <img src="img/logo/Law.png" alt="Logotipo Firma Law">
                </div>
            </div>
        </div>

<div class="fix-top">
    <div class="row">
        <div class="top-line">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>AREA DE TRABAJO</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>
    </div>
    <div>
       <form action="menu.php?id=12" method="post">
           <div class="add">
                <a data-toggle='collapse' href='#demo' data-toggle="collapse"><h2 class='title-h2'><i class="far fa-plus-square"></i> Agregar Carpeta</h2></a>
           </div>
           <div class="col-md-12 collapse" style="margin-bottom: 15px;" id="demo">
               <div class="col-md-6">
                   <input type="text" name="nombre" class="form-control upper" autofocus="" required="">
               </div>
               <div class="col-md-1" style="margin-top: 1px;">
                    <div class="boton-formulario">
                      <button type="submit" class="boton3">CREAR</button>
                    </div>                   
               </div>
           </div>
       </form> 
    </div>
</div>
<style>
.file-move {
    text-align: left;
}
.file-move-sub {
    text-align: auto;
    margin-left: 20px;
}
.mov {
    text-align: right;
}
</style>
<?php
/******************** CARPETAS DE AREA DE TRABAJO *******************************/
function listFiles($nivel_1, $conteo){
    
    if(is_dir($nivel_1)){//Primero de Verifica si es Directorio Fichero//

        if($dir = opendir($nivel_1)){//Se abre el Directorio//
            $var = 0;
            

            while(($archivos = readdir($dir))!== false){//Se hace el recorrido de lo contenido dentro del Directorio//
                $var ++;
                if($archivos != '.' && $archivos != '..'){//Omitimos los archivos del sistema//

                    $RutaNivel_1 = $nivel_1.$archivos;//Creamos una Nueva Ruta con la Anterior más la Carpeta Actual//
                    $archivos = strtoupper($archivos);
                    $nivel = 'Carpeta';
                    $folder = "<i class=\"far fa-folder fa-3x\"></i> ";
                    $add    = "<i class=\"far fa-plus-square fa-lg\"></i>";
                    $add_caso = "<i class=\"far fa-arrow-alt-circle-right fa-lg\"></i>";

                    $collapse1 = str_replace(' ','',$archivos);
        

                   echo "<div class='container' style='margin-bottom: -12px;'>
                            <div class='panel-group'>
                                <div class='panel panel-warning'>
                                    <div class='panel-heading file-move'>";

                                        echo "<div class='wrapper-main-files contenido'>";
                                                echo "<a data-toggle='collapse' href='#$archivos".$var."'> <p>$folder $archivos</p></a>";

                                                        echo "<div class='mov contenido add-folder'>";
                                                            echo "<a data-toggle='collapse' href='#$collapse1$nivel".$var."'><p>$add Agregar Carpeta</p></a>";
                                                            /*href='menu.php?id=2&tmp=$RutaNivel_1'*/
                                                                if($conteo == 1){
                                                                    echo "<span>/</span> ";
                                                                    echo "<a href='#' data-toggle='modal' data-target='#ModalCrea' style='margin-left:20px;'><p>Crear Caso  $add_caso</p></a>";
                                                                }else{
                                                                    //echo "No tiene Permiso a los Casos";
                                                                }

                                                        echo "</div>";

                                                echo "</div>";
                                        echo "</div>";

                               echo "</div>
                                    
                                </div>
                            </div>
                        ";

                    echo "<div class='container'>";
                    $img = "<img src='img/carpeta.png' width='35px;'>";


                    echo "<div id='$collapse1$nivel".$var."' class='panel-collapse collapse' style='margin-bottom: 50px;'>";
                    echo "<form action='menu.php?id=20' method='post'>";
                        echo "<input name='ruta' value='$RutaNivel_1' style='visibility: hidden;'>";
                            echo "<div style='margin-top: -25px;'>";
                                echo "<div class='col-md-5' style='margin-bottom: 10px;'>";

                                echo "<input type='text' class='form-control' name='nombre' placeholder='Ingresar Nombre de Carpeta...'>";
                                echo "</div>";
                                echo "<div class='col-md-1' style='margin-top: 1px;'>";
                                    echo "<div class='boton-formulario'>";
                                    echo "<button type='submit' class='boton3'>CREAR</button>";
                                    echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</form>";
                    echo "</div>";
                    

                    if(is_dir($RutaNivel_1)){
                        $call = 0;
                        if($dir2 = opendir($RutaNivel_1)){

                            echo "<div id='$archivos".$var."' class='panel-collapse collapse panel-success file-move'>";
                            echo "<ul class='list-group'>";
        
                            while(($archivosI = readdir($dir2))!== false){
                                $call ++;
                                if($archivosI != '.' && $archivosI != '..'){

                                    $RutaNivel_2 = $RutaNivel_1.'/'.$archivosI;
                                    $nivel2 =  $archivos.$call;
                                    $ext = new SplFileInfo($archivosI);
                                    $tipo =  $ext->getExtension();
                                    $add = "<i class=\"far fa-plus-square fa-lg\"></i> ";

                                    $nivel = 'SubCarpeta';
                                    $collapse = utf8_encode(str_replace(' ','',$archivosI));
                                    $add_archivo = "<i class=\"fas fa-upload fa-2x\"></i> ";
                                    $add_nivel = "Subir".$archivos.$call;
                                    
                                    
                                    $img = "<i class=\"far fa-folder fa-2x\"></i> ";
                                    

                                    if(($tipo == 'zip') or ($tipo == 'rar') or ($tipo == 'jpg') or ($tipo == 'png') or ($tipo == 'xlsx') or ($tipo == 'xlsm') or ($tipo == 'xls') or ($tipo == 'xltx') or ($tipo == 'docx') or ($tipo == 'txt') or ($tipo == 'doc') or ($tipo == 'pdf') or ($tipo == 'PDF')){
                                        echo "<li style='color: #ccc; class='list-group-item'>$collapse</li>";
                                    }else{
                                        echo "<div class='wrapper-main-files list-group-item contenido_niv'>";
                                            echo "<a href='#' data-toggle='collapse' data-target='#$nivel2".$collapse."' class=''>$img  $archivosI</a>";
                                            echo "<div class='mov contenido_niv'>";
                                                echo "<a data-toggle='collapse' href='#$collapse$nivel".$call."'>$add Agregar Carpeta</a>";
                                                echo "<a href='#' data-toggle='collapse' data-target='#$add_nivel' style='margin-left:20px;'>$add_archivo Subir Archivos</a>";
                                            echo "</div>";
                                        echo "</div>";
                                    }
                                    
                                    echo "<div id='$collapse$nivel".$call."' class='panel-collapse collapse' style='margin-bottom: 50px;'>";
                                    echo "<form action='menu.php?id=21' method='post'>";
                                    echo "<input name='ruta' value='$RutaNivel_2' style='visibility: hidden;'>";
                                        echo "<div style='margin-top: -25px;'>";
                                            echo "<div class='col-md-5' style='margin-bottom: 10px;'>";
                                                
                                                echo "<input type='text' class='form-control' name='nombre' placeholder='Ingresar Nombre de Carpeta...'>";
                                            echo "</div>";
                                           echo "<div class='col-md-1' style='margin-top: 1px;'>";
                                                echo "<div class='boton-formulario'>";
                                                echo "<button type='submit' class='boton3'>CREAR</button>";
                                                echo "</div>";
                                            echo "</div>";
                                        echo "</div>";
                                        echo "</form>";
                                echo "</div>"; 
                                /*este aparto es para atachar documentos a nivel 2 */
                                echo "<div id='$add_nivel' class='panel-collapse collapse' style='margin-bottom: 50px;'>";
                                echo "<form action='menu.php?id=14' method='post' enctype='multipart/form-data'>";
                                //style='visibility: hidden;'
                                echo "<input name='ruta' value='$RutaNivel_2' style='visibility: hidden;'>";
                                    echo "<div style='margin-top: -25px;'>";
                                        echo "<div class='col-md-5' style='margin-bottom: 10px;'>";
                                            
                                            echo "<input type='file' class='form-control' name='file'>";
                                        echo "</div>";
                                       echo "<div class='col-md-1' style='margin-top: 1px;'>";
                                            echo "<div class='boton-formulario'>";
                                            echo "<button type='submit' class='boton3'>SUBIR</button>";
                                            echo "</div>";
                                        echo "</div>";
                                    echo "</div>";
                                    echo "</form>";
                            echo "</div>";   


                            if(is_dir($RutaNivel_2)){
                                $call2 = 0;
                                if($dir3 = opendir($RutaNivel_2)){

                                    echo "<div id='$nivel2".$collapse."' class='panel-collapse collapse'>";
                                    echo "<ul class='list-group'>";

                                    while(($archivosII = readdir($dir3))!== false){
                                        $call2 ++;
                                        if($archivosII != '.' && $archivosII != '..'){

                                            $RutaNivel_2;
                                            $RutaNivel_3 = $RutaNivel_2.'/'.$archivosII;
                                            $archivosII;
                                            $add = "<i class=\"fas fa-upload fa-1x\"></i> ";
                                            $img = "<i class=\"far fa-folder fa-2x\"></i> ";
                                            $dowload = "<i class=\"fas fa-download fa-2x\"></i> ";

                                            $nivel3 = $archivosI.$var.$call2;
                                            
                                            $ext = new SplFileInfo($archivosII);
                                            $tipo =  $ext->getExtension();

                                            $document = utf8_decode(strtolower($archivosII));
                                            $collapse3 = str_replace(' ','',$nivel3);
                                            $remplace = str_replace(' ','',$archivosII);
                                            $add_nivel2 = "Subir".$remplace.$call2;

                                            if(($tipo == 'zip') or ($tipo == 'rar') or ($tipo == 'jpg') or ($tipo == 'JPG') or ($tipo == 'png') or ($tipo == 'xlsx') or ($tipo == 'xlsm') or ($tipo == 'xls') or ($tipo == 'xltx') or ($tipo == 'docx') or ($tipo == 'txt') or ($tipo == 'doc') or ($tipo == 'pdf') or ($tipo == 'PDF') or ($tipo == 'rtf')){
                                                        
                                                echo "<li style='color: #ccc;' class='list-group-item'><a href='$RutaNivel_3' target='_blanck' class='download'>$dowload $document</a></li>";

                                             }else{
                                                 echo "<div class='wrapper-main-files list-group-item contenido_nivel'>";
                                                     echo "<a href='#' data-toggle='collapse' data-target='#$collapse3".$call2."' class='niv_3'>$img $archivosII</a>";
                                                     echo "<div class='mov contenido_nivel'>";
                                                         echo "<a href='#' data-toggle='collapse' data-target='#$add_nivel2'>$add Subir Archivos</a>";
                                                     echo "</div>";
                                                 echo "</div>";

                                                 echo "<div id='$add_nivel2' class='panel-collapse collapse' style='margin-bottom: 50px;'>";
                                                 echo "<form action='menu.php?id=23' method='post' enctype='multipart/form-data'>";
                                                 //style='visibility: hidden;'
                                                 echo "<input name='ruta' value='$RutaNivel_3' style='visibility: hidden;'>";
                                                     echo "<div style='margin-top: -25px;'>";
                                                         echo "<div class='col-md-5' style='margin-bottom: 10px;'>";
                                                             
                                                             echo "<input type='file' class='form-control' name='file'>";
                                                         echo "</div>";
                                                        echo "<div class='col-md-1' style='margin-top: 1px;'>";
                                                             echo "<div class='boton-formulario'>";
                                                             echo "<button type='submit' class='boton3'>SUBIR</button>";
                                                             echo "</div>";
                                                         echo "</div>";
                                                     echo "</div>";
                                                     echo "</form>";
                                             echo "</div>";                                                   

                                             } 
                                     


                                             if(is_dir($RutaNivel_3)){
                                                if($dir4 = opendir($RutaNivel_3)){

                                                    echo "<div id='$collapse3".$call2."' class='panel-collapse collapse'>";
                                                    
                                                    while(($archivosIII = readdir($dir4))!== false){
                                                        if($archivosIII != '.' && $archivosIII != '..'){
                                                            $RutaNivel_4 = $RutaNivel_3.'/'.$archivosIII;
                                                            $archivosIII;
                                                            $dowload = "<i class=\"fas fa-download fa-2x\"></i> ";

                                                            $ext = new SplFileInfo($archivosIII);
                                                            $tipo =  $ext->getExtension();

                                                            if(($tipo == 'zip') or ($tipo == 'rar') or ($tipo == 'jpg') or ($tipo == 'JPG') or ($tipo == 'png') or ($tipo == 'xlsx') or ($tipo == '.xlsm') or ($tipo == 'xls') or ($tipo == 'xltx') or ($tipo == 'docx') or ($tipo == 'txt') or ($tipo == 'doc') or ($tipo == 'pdf') or ($tipo == 'PDF') or ($tipo == 'rtf')){
                                                                echo "<ul class='list-group'>";
                                                                    echo "<li style='color: #ccc;' class='list-group-item'><a href='$RutaNivel_4' target='_blanck' class='download_niv_3'>$dowload $archivosIII</a></li>";
                                                                echo "</ul>";

                                                            }else{

                                                            }

                                                        }
                                                    }
                                                    
                                                    echo "</div>";

                                                }
                                             }

                                        }
                                    }

                                    echo "</div>";
                                    echo "</ul>";

                                }
                            }                            
                                }
                            }
                            echo "</div>";
                            echo "</ul>";
                        }
                    }

                    echo "</ul>";
                echo "</div>";
                }
                
            }
        }
    }else{  }//echo "No es Directorio el Nivel 1"; }
}
echo "</div>";

listFiles("CASOS/", $res_caso1);

?>
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, CONCAT(NOMBRES,' ',APELLIDOS)NOMBRES
								FROM tb_contacto");

$area = mysqli_query($conn,"SELECT ID_AREA, DESCRIPCION
								FROM tb_area");

$busqueda	= mysqli_query($conn, "SELECT CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, A.DESCRIPCION, DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), 
									A.ID_CONTACTO, A.ID_CASO, A.OBSERVACIONES
									FROM 	tb_caso A,
											tb_contacto B
									WHERE A.ID_CONTACTO = B.ID_CONTACTO");

$sql = mysqli_query($conn, "SELECT NOMBRES, APELLIDOS, CUI, TELEFONO, DIRECCION, ZONA, EMAIL, ID_CONTACTO
							FROM tb_contacto");
						 
?>
<div class=" bajar">
		<div class="row">

		<!--div class="col-md-12 bajar">
			<div class="boton-formulario">
			<center>
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">CREAR NUEVO CASO</button>
			</center>
			</div>			
		</div--> 			

		</div>
	</div>

	<div class="">
	<div class="modal fade" id="ModalCrea" role="dialog">
		<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CREACI&Oacute;N DE CASOS</h4>
			</div>
			<div class="modal-body cuerpo">

			<form action="menu.php?id=30" method="post" enctype="multipart/form-data" onsubmit="return validar();">
			
			

			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab">GENERALES CASO</a></li>
					<li><a href="#tab2" data-toggle="tab">DATOS JUZGADOS</a></li>
				</ul>
        <div class="tab-content">
			<div class="tab-pane active" id="tab1">
			<div>
					<label>CONTACTO</label>
						<select name="id_contacto" class="form-control" autofocus="">
							<option value="">SELECCIONAR CONTACTO</option>
						<?php
						while ($row = mysqli_fetch_array($contacto))
						{
							echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['NOMBRES'] . '</option>' . "\n";
						}
						?>
						</select>
					</div>
					<div>
					<?php

					echo "<label for=''>RUTA DE CASO</label>";
									echo "<select name='ruta' id='ruta' class='form-control upper'>";

					function listar_archivos($carpeta){
						if(is_dir($carpeta)){
							if($dir = opendir($carpeta)){
								while(($archivo = readdir($dir)) !== false){
									if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
										$archivo = $archivo;
										echo "<option value='$carpeta$archivo'>$archivo</option>";
									}
								}
								closedir($dir);
							}
						}
						}
					
						echo listar_archivos(__DIR__."/CASOS/");


							
						echo "</select>";
				?>

					</div>

					<div>
						<label>CASO ORIGEN</label>
						<select name="origen" class="form-control">
							<option value="0">SIN CASO ORIGEN</option>
							<?php
								$sdl = mysqli_query($conn, "SELECT ID_CASO, CAUSA
															FROM tb_caso");
								while($res = mysqli_fetch_array($sdl)){
									echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
								}							
							?>
						</select>
					</div>                    
					
					<div>
						<label>DESCRIPCION</label>
						<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
					</div>
					<div>
						<label>FECHA INICIO</label>
						<input type="text" name="fec_inicio" class="form-control centrar" id="datepicker" placeholder="FECHA INICIO" required="">
					</div>
					<div>
						<label>FECHA FINAL</label>
						<input type="text" name="fec_final" class="form-control centrar" id="datepicker_1" placeholder="FECHA FINAL">
					</div>
					<div>
						<label>OBSERVACIONES</label>
						<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
					</div>

					<div>
						<label for="">ADJUNTAR ARCHIVO</label>
						<input type="file" name="file" class="form-control">
					</div>


			</div>
			<div class="tab-pane" id="tab2">
				<div>
					<label>CAUSA</label>
					<input type="text" name="causa" class="form-control upper" placeholder="Causa de Caso" id="causa">
				</div>
				<div>
					<label>N&Uacute;MERO MP</label>
					<input type="text" name="mun_mp" class="form-control upper" placeholder="N&uacute;mero MP">
				</div>
				<div>
					<label>FISCALIA</label>
					<input type="text" name="fiscalia" class="form-control upper" placeholder="Fiscalia">
				</div>
				<div>
					<label>JUZGADO</label>
					<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
				</div>
				<div>
					<label>JUEZ</label>
					<input type="text" name="juez" class="form-control upper" placeholder="Juez">
				</div>
				<div>
					<label>OFICIAL</label>
					<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
				</div>	
				<div>
					<label>DIRECCI&Oacute;N JUZGADO</label>
					<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
				</div>	
				<div>
					<label>ZONA JUZGADO</label>
					<select name="zona_juz" class="form-control">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>						
					</select>
				</div>
				<div>
					<label>TELEFONO JUZGADO</label>
					<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
				</div>															
			</div>
        </div>
		<div class="boton-formulario bajar">
						<button type="submit" class="boton3">GRABAR</button>
						<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
					</div>
		</form>
        </div>
				<!--form action="inserta_caso.php" method="post" enctype="multipart/form-data">
					
				</form-->
			</div>
		</div>
		
		</div>
	</div>
</div> 