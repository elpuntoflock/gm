<?php
require("events_calendar/functions/functions.php");

// CSRF Protection
require 'events_calendar/functions/CSRF_Protect.php';
$csrf = new CSRF_Protect();

// Error Reporting Active
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<link href="events_calendar/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">	
	<!-- DataTables CSS -->
    <link href="events_calendar/css/dataTables.bootstrap.css" rel="stylesheet">	
	<!-- FullCalendar CSS -->
	<link href="events_calendar/css/fullcalendar.css" rel="stylesheet" />
	<link href="events_calendar/css/fullcalendar.print.css" rel="stylesheet" media="print" />	
	<!-- jQuery -->
    <script src="events_calendar/js/jquery.js"></script>	
	<!-- SweetAlert CSS -->
	<script src="events_calendar/js/sweetalert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="events_calendar/css/sweetalert.css">
    <!-- Custom Fonts -->
    <link href="events_calendar/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<!-- ColorPicker CSS -->
	<link href="events_calendar/css/bootstrap-colorpicker.css" rel="stylesheet">



</head>

	<body>

	<!-- Page Content -->
		<div class="content-section-a bajar">
			
			<!--BEGIN PLUGIN -->
			<div class="container">

		<div class="">
			<div class="row">
				<div class="col-md-12">
					<div class="wrapper-logo-secondary">
						<img src="img/logo/Law.png" alt="Logotipo Firma Law">
					</div>
				</div>
			</div>
		</div>

			<div class="wrapper-return">
				<button type="button" class="boton4"><a href="menu.php?id=8">Regresar</a></button>
			</div>

				<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
							<div class="col-md-4" data-line="movil"><div class="line"></div></div>
							<div class="col-md-4 titulo-seccion bajar_espacio"><p>EDITAR TAREAS</p></div>
							<div class="col-md-4"><div class="line"></div></div>
						</div>
				<div class="row bajar">
				    <div class="col-lg-12">
						

						<div class="bajar">
						 <form id="editEvent" method="post" enctype="multipart/form-data" class="form-horizontal" name="editEvent">
						 
							<!-- Text input-->
							<!--div class="form-group">
								<label class="col-md-3 control-label" for="title">SELECCIONAR TIPO</label>
								<div class="col-md-6">
									<select name='title' class="form-control input-md">
										
										<?php 
										
										$query = mysqli_query($conection, "select * from type ORDER BY id DESC");
										
											echo "<option value='No tiene Tipo' required>SELECCIONAR TIPO</option>";
											
										while ($row = mysqli_fetch_assoc($query)) {
											  
											echo "<option value='".$row['title']."'>".$row['title']."</option>";
																
										  }
									
										?>
									</select>
								</div>
							</div-->

							<?php echo editEvent($_GET['rc']); ?>
							<?php $csrf->echoInputField(); ?>
							
							<!-- Button -->
							<div class=''>
								<input type='submit' name='editEvent' class='boton3' value='EDITAR' />
							</div>

							</fieldset>
						</form>

					</div>
				  </div>
			<?php
	
			if(isset($_POST['editEvent']))
				{
					
					updateEvent($_GET['rc'],strtoupper($_POST['title']),strtoupper($_POST['description']),strtoupper($_POST['observaciones']),$_POST['start'],$_POST['end'],$_POST['url'],$_POST['color']);
					
				}
				
			?>
			<!-- Modal with events description -->
			<?php echo modalEvents(); ?>
				</div>

			</div>
			<!-- /.container -->

		</div>



		<!-- Bootstrap Core JavaScript -->
		
		<!-- DataTables JavaScript -->
		<script src="events_calendar/js/jquery.dataTables.js"></script>
		<script src="events_calendar/js/dataTables.bootstrap.js"></script>
		<!-- Listings JavaScript delete options-->
		<script src="events_calendar/js/listings.js"></script>
		<!-- Metis Menu Plugin JavaScript -->
		<script src="events_calendar/js/metisMenu.min.js"></script>
		<!-- Moment JavaScript -->
		<script src="events_calendar/js/moment.min.js"></script>
		<!-- FullCalendar JavaScript -->
		<script src="events_calendar/js/fullcalendar.min.js"></script>
		<!-- FullCalendar Language JavaScript Selector -->
		<script src='events_calendar/lang/en-gb.js'></script>
		<!-- DateTimePicker JavaScript -->
		<script type="text/javascript" src="events_calendar/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<!-- Datetime picker initialization -->
		<script type="text/javascript">
			$('.form_date').datetimepicker({
				language:  'en',
				weekStart: 1,
				todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0
			});
		</script>	
		<!-- ColorPicker JavaScript -->
		<script src="events_calendar/js/bootstrap-colorpicker.js"></script>
		<!-- Plugin Script Initialization for DataTables -->
		<script>
			$(document).ready(function() {
				$('#dataTables-example').dataTable();
			});
		</script>
		<!-- ColorPicker Initialization -->
		<script>
			$(function() {
				$('#cp1').colorpicker();
			});
		
		</script>
		<!-- JS array created from database -->
		<?php echo listEvents(); ?>
		
		
	</body>

</html>

