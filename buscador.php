<?php
include('seguridad.php');
?>
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery.js"></script>
<script src="js/jquery.dataTables.min.js"></script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion"><p>BUSQUEDA DOCUMENTOS</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="col-md-12 table-responsive bajar">

<div class="col-md-2"></div>
    <div class="col-md-8">
        <header>
            <input type="search" id="input-search" class="form-control center" placeholder="Buscar archivos aqui...!!!" autofocus="">
            <div class="content-search">
                <div class="content-table">
                    <table id="table" class="col-md-8">
                        <thead>
                            <td></td>
                        </thead>
                        <tbody>
                            <?php
                                
                                $usuario 	= $_SESSION['usuario'];
                                $usuario 	= strtoupper($usuario);
                                require_once('db/conexion.php');

                                $sql = mysqli_query($conn, "SELECT a.id_caso, a.descripcion, a.ruta
                                                                FROM tb_documento a,
                                                                    tb_acceso b,
                                                                    tb_usuario c
                                                                WHERE a.id_caso = b.ID_CASO     
                                                                AND b.ID_USUARIO = c.ID_USUARIO
                                                                AND c.ID_USUARIO = '".$usuario."'");
                
                                while ($row = mysqli_fetch_array($sql)){
                                    
                                    $info = new SplFileInfo(strtoupper($row[1]));
                                    ///echo $info;
                                    $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);
        
                                    if($extension == 'XLS' OR $extension == 'XLSX'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/excel.jpg' />";
                                    }elseif($extension == 'PNG' OR $extension == 'JPG'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/imagenes.png' />";
                                    }elseif($extension == 'SQL'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/sql.png' />";
                                    }elseif($extension == 'PDF'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/pdf.png' />";
                                    }elseif($extension == 'DOC' OR $extension == 'DOCX'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/word.png' />";
                                    }elseif($extension == 'TXT'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/txt.png' />";
                                    }                                    

                                    echo "<tr>";
                                        echo "<td><a href='$row[2]' target='_blanck'>$extension $row[1]</a></td>";
                                    echo "</tr>";
                                }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </header>
    </div>
</div>
<script src="js/search.js"></script>