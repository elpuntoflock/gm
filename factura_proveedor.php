<?php
require_once 'db/conexion.php';

$contacto = mysqli_query($conn,"SELECT DISTINCT ID_PROVEEDOR, CONCAT_WS(' ', NOMBRE_PROVEEDOR,APELLIDO_PROVEEDOR,'-',NOMBRE_EMPRESA)NOMBRES
                                FROM tb_proveedor
                                ORDER BY 2");
?>

<style>
.wrapper-ocultar {
    display: none;
}

</style>

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>


<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: -15px !important;"><p>FACTURA PROVEEDORES</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="row">
    <form action="menu.php?id=52" method="post">
        <div class="col-md-12">
            <div class="col-md-4">
                <label for="">PROVEEDOR</label>
                <select name="proveedor" id="proveedor" class="form-control" required="">
                    <option value="">SELECCIONAR</option>
                    <?php
                        while ($row = mysqli_fetch_array($contacto))
                        {
                            echo '<option value="' . $row['ID_PROVEEDOR']. '">'. $row['NOMBRES'] . '</option>' . "\n";
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-3">
                <label for="">TIPO DOCUMENTO</label>
                <select name="tipo_documento" id="tipo_doc" class="form-control" >
                    <option value="">SELECCIONAR</option>
                    <option value="F">FACTURA</option>
                    <option value="R">RECIBO</option>
                </select>
            </div>
            <div id="tipo_pago" class="col-md-4 wrapper-ocultar">
                <label for="">TIPO DE PAGO</label>
                <select name="tipo_pago" id="pago" class="form-control" autofocus="">
                    <option value="">SELECCIONAR</option>
                    <option value="1">CONTADO</option>
                    <option value="2">CREDITO</option>
                </select>
            </div>            
        </div>

        <div class="col-md-12 bajar">
            <div class="col-md-4">
                <label for="">NOMBRE PROVEEDOR</label>
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre Proveedor" readonly="">
            </div>

            <div class="col-md-2" id="oculta_docu">
                <label for="">SERIE</label>
                <input type="text" name="serie" id="serie" class="form-control upper center" placeholder="Serie">
            </div> 

            <div class="col-md-2">
                <label for="">FACTURA</label>
                <input type="text" name="factura" id="factura" class="form-control upper center" placeholder="Factura">
            </div>

            <div class="col-md-2" id="oculta_dias">
                <label for="">DIAS</label>
                <input type="text" name="dias" id="dias" class="form-control upper center" placeholder="Dias">
            </div>            

            <div class="col-md-2">
                <label for="">FECHA</label>
                <input type="text" name="fecha" id="date" class="form-control upper center" placeholder="Fecha">
            </div>                                    
        </div>

        <div class="col-md-12">
            <div class="col-md-9">
                <label for="">DESCRIPCI&Oacute;N</label>
                <input type="text" name="descripcion" class="form-control upper" placeholder="Descripci&oacute;n">
            </div>

            <div class="col-md-3">
                <label for="">MONTO</label>
                <input type="text" name="monto" class="form-control upper center" placeholder="MONTO">
            </div>            
        </div>

    <div class="col-md-12 bajar">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="boton3">GRABAR</button>
        </div>
    </div>


    </form>
</div>

<script src="js/jquery.min.js"></script>

<script>
    $(document).ready(function(){

        $('#proveedor').change(function(){

            var proveedor = $('#proveedor').children('option:selected').val();
            
            $.post('datos_proveedor.php', {proveedor: proveedor}).done(function( respuesta )
            {
                $('#nombre').val(respuesta);
            })     
        })
    })

     $("#tipo_doc").change(function(){

        var tipo_docu = document.getElementById('tipo_doc').value;
        

        if(tipo_docu == 'R'){
            $('#serie').attr('readonly',true);
            $('#dias').attr('readonly',true);
            $('#factura').attr('readonly',true);
        }else{
            $('#serie').attr('readonly',false);
            $('#dias').attr('readonly',false);
            $('#factura').attr('readonly',false);
        }

        if(tipo_docu == 'F'){
            $('#tipo_pago').removeClass('wrapper-ocultar');
        }else{
            $('#tipo_pago').addClass('wrapper-ocultar');
        }

        

     })

     $("#pago").change(function(){

        var tipo_pago = document.getElementById('pago').value;

        if(tipo_pago == '1'){
            $('#dias').attr('readonly',true);
        }else{
            $('#dias').attr('readonly',false);
        }

     })
</script>
<script>
    $( function() {
        $( '#date' ).datepicker();
    } );
</script>