<?php
require_once('lib/pdf/mpdf.php');
/*Conexion a la Base de Datos*/
require_once('db/conexion.php');

$contacto = $_REQUEST['tmp'];
$moneda   = $_REQUEST['view'];
$fecha = date('d/m/Y');


$detalle = mysqli_query($conn, "SELECT B.ID_CASO, B.DESCRIPCION, A.SALDO, B.CAUSA, A.MONEDA
								FROM tb_corriente A,
									   tb_caso B,
								     tb_contacto C
								WHERE A.ID_CASO 	= B.ID_CASO
								  AND B.ID_CONTACTO = C.ID_CONTACTO
                  AND C.ID_CONTACTO = '".$contacto."'
                  AND A.MONEDA = '".$moneda."'");
                                  
    while ($rowX = mysqli_fetch_array($detalle)){
        
        $loop = $loop .'
        <tr>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[3].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[1].'</td>
        <td style="font-weight: bold; color: #000 !important; text-align: right;">'.$rowX[4].'&nbsp;&nbsp;'.number_format($rowX[2],2,'.',',').'</td>
        </tr>
        ';
    
    }  

    $datos = mysqli_query($conn, "SELECT ID_CONTACTO, CONCAT(NOMBRES,' ',APELLIDOS)NOMBRES
                                    from tb_contacto
                                    where id_contacto = '".$contacto."'");

while($resdat = $datos->fetch_array(MYSQLI_ASSOC)){

  
  $contacto_c     = $resdat['NOMBRES'];
  

}   


$tot = mysqli_query($conn, "SELECT sum(A.SALDO)DETALLE, A.MONEDA
                              FROM tb_corriente A,
                                    tb_caso B,
                                  tb_contacto C
                              WHERE A.ID_CASO 	  = B.ID_CASO
                                AND B.ID_CONTACTO = C.ID_CONTACTO
                                AND C.ID_CONTACTO = '".$contacto."'
                                AND A.MONEDA 		  = '".$moneda."'");

   while($array = $tot->fetch_array(MYSQLI_ASSOC)){

    $detalle    = number_format($array['DETALLE'],2,'.',',');
    $det_moneda = $array['MONEDA'];
                                  
   }                          
    
    $html = "<header class='clearfix'>

    <h1>REPORTE CONSOLIDADO POR CLIENTE</h1>
    <br>
    <br>
    <div style='text-align: right;'>Fecha de Impresi&oacute;n: $fecha</div>
    <br>
    <br>
    <div id='logo'>
    <img src='img/logo/Law.png' style='width: 150px;'>
    </div>
    <br>
    <br>
    <br>
    <div>
      <ul>
        <li>Cliente:<span> $contacto_c</span></li>
      </ul>
    </div>
    
    
    </header>
    <main>
    <!--Datos de Encabezado-->
    <table>
    <thead>
    <tr style='background-color: #005691;'>
    <th class='service' style='color: #fff; text-align: center;'>CAUSA</th>
    <th class='desc' style='text-align: center; color: #fff; text-align: center;'>DESCRIPCI&Oacute;N DE CAUSA</th>
    <th class='service' style='color: #fff; text-align: center;'>SALDO</th>
    
    </tr>
    </thead>
    <tbody>
    $loop;
    <br>
      <tr style='background-color: #005691;'>
        <td style='color: #fff; text-align: center;'>SALDO A LA FECHA</td>
        <td style='color: #fff; text-align: center;'>$fecha</td>
        <td style='color: #fff; text-align: center; text-align: right;'>$det_moneda&nbsp;&nbsp;$detalle</td>
      </tr>
    </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style='text-align:center;'>Firma:___________________________________</div>
    <div style='text-align:center;'>Lic. Victor P&eacute;rez</div>
    
    </main>";  
  
  $mpdf = new mPDF('c','A4');
$css = file_get_contents('lib/reportes/css/style.css');
$mpdf->writeHTML($css,1);
$mpdf->WriteHTML(utf8_encode($html));
//$mpdf->writeHTML($html);
$mpdf->Output('reporte.pdf','I');

?>