<?php
require_once 'db/conexion.php';

$sql = mysqli_query($conn, "SELECT A.IDCUENTA, B.BANCO, A.CUENTA, A.NUM_CUENTA, A.MONEDA
                                FROM tb_cuenta_banco A,
                                    tb_banco B
                                WHERE a.ID_BANCO = B.ID_BANCO");

$sql_banco = mysqli_query($conn, "SELECT ID_BANCO, BANCO
                                    FROM tb_banco");                                

?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: -15px !important;"><p>CUENTAS BANCARIAS</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>


<div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th>CORRELATIVO</th>
                    <th>BANCO</th>
                    <th>CUENTA</th>
                    <th>NUMERO CUENTA</th>
                    <th>MONEDA</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while ($row = mysqli_fetch_array($sql)){
                    echo "<tr>";
                        echo "<td>";
                            echo $row[0];
                        echo "</td>";
                        echo "<td>";
                            echo $row[1];
                        echo "</td>";  
                        echo "<td>";
                            echo $row[2];
                        echo "</td>";  
                        echo "<td>";
                            echo $row[3];
                        echo "</td>";  
                        echo "<td>";
                            echo $row[4];
                        echo "</td>";                                                                                          
                    echo "</tr>";
                }
            ?>
            </tbody>
        </table>
</div>

	<div class=" bajar">
		<div class="row">
			<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
				<div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>INGRESO CUENTAS</p></div>
				<div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			</div> 

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">NUEVAS CUENTAS</button>
			</div>			
		</div>

  <div class="modal fade" id="ModalCrea" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Ingreso Cuentas Bancarias</h4>
        </div>
        <div class="modal-body">
            <form action="menu.php?id=54" method="post">
                <div>
                    <label for="">SELECCIONAR BANCO</label>
                    <select name="banco" class="form-control" autofocus="">
                    <?php
                    while ($row = mysqli_fetch_array($sql_banco))
                    {
                        echo '<option value="' . $row['ID_BANCO']. '">' . $row['BANCO'] . ' </option>' . "\n";
                    }
                    ?>
                    </select>
                </div>
                <div>
                    <label for="">NOMBRE CUENTA</label>
                    <input type="text" name="nombre" class="form-control upper" placeholder="Nombre Cuenta">
                </div>
                <div>
                    <label for="">NO. CUENTA</label>
                    <input type="text" name="num_cuenta" class="form-control upper" placeholder="INgresar Cuenta">
                </div>
                <div>
                    <label for="">MONEDA</label>
                    <select name="moneda" class="form-control">
                        <option value="Q">QUETZALES</option>
                        <option value="$">DOLARES</option>
                    </select>
                </div>

                <div class="boton-formulario bajar">
                    <button type="submit" class="boton3">GRABAR</button>
                    <button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
                </div> 

            </form>
        </div>
      </div>
      
    </div>
  </div>        