<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
include('seguridad.php');
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

# CM Se modifica a CONCAT_WS para concatenar aunque lleve null algun campo, se ordena por campo concatenado

$contacto = mysqli_query($conn,"SELECT DISTINCT A.ID_CONTACTO, CONCAT_WS(' ', A.NOMBRES,A.APELLIDOS,'-',A.NOMBRE_EMPRESA)NOMBRES
                              FROM tb_contacto A,
                                   tb_caso B
                              WHERE A.ID_CONTACTO = B.ID_CONTACTO
                              ORDER BY 2");

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM       = '".$_REQUEST['id']."'");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}                                

?>
<style>
.wrapper-ocultar {
    display: none;
}

</style>


    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>


<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: 2px !important;"><p>FACTURACI&Oacute;N</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="row">
    <form action="menu.php?id=44" method="post" onSubmit="return validacion();">

        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-4">
                <label for="">TIPO DE PAGO</label>
                <select name="tipo_pago" id="tipo_pago" class="form-control" autofocus="" required="">
                    <option value="">SELECCIONAR</option>
                    <option value="1">CONTADO</option>
                    <option value="2">CREDITO</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="">TIPO DOCUMENTO</label>
                <select name="tipo_documento" id="tipo_doc" class="form-control" required="">
                    <option value="">SELECCIONAR</option>
                    <option value="F">FACTURA</option>
                    <option value="R">RECIBO</option>
                </select>
            </div>
        </div>

        <div class="col-md-12 bajar">
            <div class="col-md-1" id="oculta_docu">
                <label for="">SERIE</label>
                <input type="text" name="serie" id="serie" class="form-control upper center" placeholder="Serie">
            </div>
            <div class="col-md-2" >
                <label for="">FACTURA/RECIBO</label>
                <input type="text" id="factura" name="factura" class="form-control upper" placeholder="FACTURA" onChange="return concat();">
            </div>
            <div class="col-md-2 wrapper-ocultar" id="ocultar">
                <label for="">D&Iacute;AS CREDITO</label>
                <input type="text" name="dias" class="form-control upper center" placeholder="D&Iacute;AS">
            </div>
            <div class="col-md-2 centrar">
                <label for="">FECHA</label>
                <input type="text" name="fecha" class="form-control upper center" id="datepicker" placeholder="Fecha" required="">
            </div>
            <div class="col-md-2 centrar">
                <label for="">TOTAL</label>
                <input type="text" name="total" id="monto_total" class="form-control upper center" placeholder="Total" required="">
            </div>
            <div class="col-md-1 centrar">
                <label for="">TC</label>
                <input type="text" id="tc" class="form-control upper center" name="tc" onChange="sumar (this.value);">
            </div>
            <div class="col-md-1 centrar">
                <label for="">TE</label>
                <input type="text" id="te" class="form-control upper center" name="te" onChange="sumar (this.value);">
            </div>
            <div class="col-md-1 centrar">
                <label for="">TTC</label>
                <input type="text" id="ttc" class="form-control upper center" name="ttc" onChange="sumar (this.value);">
            </div>
            <span id="monto"></span>
        </div>

       <div class="col-md-12">
            <div class="col-md-3">
                <label for="">CLIENTE</label>
                <select name="cliente" id="cliente" class="form-control" required="">
                    <option value="">SELECCIONAR</option>
                <?php
                    while ($row = mysqli_fetch_array($contacto))
                    {
                        echo '<option value="' . $row['ID_CONTACTO']. '">'. $row['NOMBRES'] . '</option>' . "\n";
                    }
				?>
                </select>
            </div>
            <div class="col-md-3">
                <label for="">NOMBRE</label>
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="NOMBRE" readonly="">
            </div>
            <div class="col-md-4">
                <label for="">DIRECCI&Oacute;N</label>
                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="DIRECCI&Oacute;N" readonly="">
            </div>
            <div class="col-md-2">
                <label for="">NIT</label>
                <input type="text" id="nit" name="nit" class="form-control center" placeholder="NIT" readonly="">
            </div>            
        </div>

                    <!--CORREGIR MAÑANA Y HACER EL INSERT-->
            <div class="col-md-12">
                <div class="col-md-12">
                <label for="">OBSERVACIONES</label>
                <input type="text" id="observa_encabeza" name="observa_encabeza" class="form-control upper" placeholder="observaciones">
                </div>
                
            </div>


        <div class="col-md-12 wrapper-title-detalle">
            <div class="top-line">
                <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
                <div class="col-md-4 titulo-seccion" style="margin-top: 2px !important;"><p>DETALLE FACTURA</p></div>
                <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
            </div>        
        </div>

    <div class="col-md-12 table-responsive bajar">
	    <table id="dynamic_field" class="display nowrap table table-striped table-bordered" style="width:100%;">
	        <thead>
	            <tr>
					<th class="centrar">CASO</th>
	                <th class="centrar">DESCRIPCI&Oacute;N CASO</th>
	                <th class="centrar">OBSERVACIONES</th>
                    <th class="centrar">SALDO CASO</th>
	                <th class="centrar">TOTAL</th>
					<th class="centrar">+ Detalle</th>
	            </tr>
	        </thead>
	        <tbody>
                <tr>
                    <td>
                        <div class="col-md-2">
                            <select name="caso[]" id="caso" data="caso" class="form-control" style="width: 200px;">
                                <option value=""></option>
                            </select>
                        </div>                        
                    </td>
                    <td>
                        <div class="col-md-4">
                            <input type="text" name="descripcion[]" id="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N CASO" readonly="" style="width: 250px;">
                        </div>                        
                    </td>
                    <td>
                        <div class="col-md-4">
                        <input type="text" name="observaciones[]" id="observaciones" class="form-control upper" placeholder="OBSERVACIONES CASO" style="width: 250px;">
                        </div>                        
                    </td>
                    <td>
                        <div class="col-md-4">
                        <input type="text" name="saldo_caso[]" id="saldo_caso" class="form-control input-table center" style="width: 100px;" readonly="">
                        </div>                        
                    </td>                    
                    <td>
                        <div class="col-md-4">
                            <input type="text" name="moneda[]" id="moneda0" class="form-control center monto" data="moneda" placeholder="TOTAL" style="width: 150px;" required="">
                        </div>                        
                    </td>  
                    <td>
                        <div class="col-md-4">
                            <button type="button" name="add" id="add" class="boton_add">+</button>
                        </div>                        
                    </td>                                                          
                </tr>
	        </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9"></div>
            <div class="col-md-3">
            <label for="">TOTAL:</label>
            <input type="text" name="suma" value="" id="test" class="form-control total center wrapper-total" readonly="">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="boton3">GRABAR</button>
        </div>

        <div class="col-md-2 ocultar-boton" id="mostrar-boton">

            <?php
            if(isset($_REQUEST['num_fac']))
                {
                $var = $_REQUEST['num_fac'];  
                echo "<button type='button' class='boton3'><a href='factura_pdf.php?num_factura=$var' target='_blank'>IMPRESI&Oacute;N</a></button>";
                }
                else{
                    $var = '0';    
                }

            ?>
           
        </div>
    </div>
    


    </form>
</div>

<script src="js/jquery.min.js"></script>

<script>
    $(document).ready(function(){
        var i=0;
        
        $('#cliente').change(function(){
            var cliente = $('#cliente').children('option:selected').val();
            
            document.cookie = cliente;

            $.post('datos_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#nombre').val(respuesta);
            })
            $.post('address_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#direccion').val(respuesta);
            })
            $.post('nit_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#nit').val(respuesta);
            })
            $.post('casos_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('select[data="caso"]').html(respuesta);
            })        
        })
        
        $('#caso').change(function(){

            var caso = $('#caso').children('option:selected').val();

            $.post('desc_casos.php', {caso: caso}).done(function( respuesta )
            {
                $('#descripcion').val(respuesta);
            })
            $.post('saldo_caso.php', {caso: caso}).done(function( respuesta )
            {
                $('#saldo_caso').val(respuesta);
            })            

        })


        $('#moneda0').change(function(){

            var monto_ingresado = $('#moneda0').val();
            
            var saldo_caso      = $('#saldo_caso').val();

            if(monto_ingresado > saldo_caso){
                alert('El monto del Caso es Mayor al Saldo...!!!! VERIFICAR...');
                return false;
            }else{

            }

        })



        $(".monto").change(function(){
            var total = 0;
            
            $(".monto").each(function() {

            if (isNaN(parseFloat($(this).val()))) {

            total += 0;

            }else{

                total += parseFloat($(this).val());
            }
            });

            document.getElementById('test').value = total;
        })
        
     
        $('#add').click(function(){
            i++;
            var contacto = document.cookie;
            var x = contacto.substring(0,1);
            
            $('#dynamic_field').append('<tr id="row'+i+'"><td><div class="col-md-2"><select name="caso[]" id="caso'+i+'" data="caso'+i+'" class="form-control" style="width: 200px;"><option value="-1">SELECCIONAR CASO</option></select></div></td><td><div class="col-md-4"><input type="text" name="descripcion[]" id="descripcion'+i+'" class="form-control upper" placeholder="DESCRIPCI&Oacute;N CASO" readonly="" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="observaciones[]" id="observaciones'+i+'" class="form-control upper" placeholder="OBSERVACIONES CASO" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="saldo_caso[]" id="saldo_caso'+i+'" class="form-control input-table center" style="width: 100px;" readonly=""></div></td><td><div class="col-md-4"><input type="text" name="moneda[]" id="moneda'+i+'" class="form-control center monto" data="moneda'+i+'" placeholder="TOTAL" style="width: 150px;" required=""></div></td><td><button type="button" id="'+i+'" class="boton_close btn_remove" name="remove">-</button></td></tr>');
            
           
            $.post('caso_cliente_x.php', {x: x}).done(function( respuesta )
            {
                $('select[data="caso'+i+'"]').html(respuesta);
            }) 
             


        $("#caso"+i+"").change(function(){

            var caso = $("#caso"+i+"").children('option:selected').val();
            
            $.post("desc_casos.php", {caso: caso}).done(function( respuesta )
            {
                $("#descripcion"+i+"").val(respuesta);
            })

            $.post('saldo_caso.php', {caso: caso}).done(function( respuesta )
            {
                $("#saldo_caso"+i+"").val(respuesta);
            })  

        })  
        
        $(".monto").change(function(){
            var total = 0;
            
            $(".monto").each(function() {

            if (isNaN(parseFloat($(this).val()))) {

            total += 0;
            
            }else{

                total += parseFloat($(this).val());
            }
            });

            document.getElementById('test').value = total;
        })        


        })

        $(document).on('click','.btn_remove', function (){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        })

       

    })
</script>

<script>
    function validacion(){
        var tc = document.getElementById('tc').value;
        var te = document.getElementById('te').value;
        var ttc = document.getElementById('ttc').value;
        var monto = document.getElementById('monto_total').value;
        var fecha = document.getElementById('datepicker').value;

        var total = document.getElementById('test').value;

        suma = parseInt(tc)+parseInt(te)+parseInt(ttc);


        if(total != monto){
            alert('El Total no coincide con los montos...');
            return false;
        }
        
    }
</script>

<script>
    $("#tipo_pago").change(function(){
        var tipo_pago = document.getElementById('tipo_pago').value; 
        
        if(tipo_pago == 1){
            $('#ocultar').addClass('wrapper-ocultar');
            $('#tc').attr('readonly',false);
            $('#te').attr('readonly',false);
            $('#ttc').attr('readonly',false);
            $('#serie').attr('required',true);
            $('#factura').attr('required',true);/**/
            $('#monto_total').attr('readonly',true);
        }
        if(tipo_pago != 1){
            $('#ocultar').removeClass('wrapper-ocultar');
            $('#tc').attr('readonly',true);
            $('#te').attr('readonly',true);
            $('#ttc').attr('readonly',true);
            $('#serie').attr('required',false);
        }

        if(tipo_docu == 'R'){
            $('#ocultar').removeClass('wrapper-ocultar');
        }


        })

    $("#tipo_doc").change(function(){
        var tipo_docu = document.getElementById('tipo_doc').value;

        if(tipo_docu == 'R'){
            $('#oculta_docu').addClass('wrapper-ocultar');
            $('#factura').attr('readonly',true);
            $('#serie').attr('required',false);
        }

        if(tipo_docu != 'R'){
            $('#oculta_docu').removeClass('wrapper-ocultar');
            $('#factura').attr('readonly',false);
            $('#serie').attr('required',true);
        }

        })     


        $('#boton').click(function(){
            $('#mostrar-boton').removeClass('ocultar-boton');
        })

</script>

<script>
    function sumar (valor){
        var total = 0;
        valor = parseInt(valor); //Convierte el valor en un entero (número).

        total = document.getElementById('monto_total').value;

        total = (total == null || total == undefined || total == "") ? 0 : total;

        total = (parseInt(total) + parseInt(valor));

        document.getElementById('monto_total').value = total;
    }
</script>

<script>
    function concat(){
        var serie   = document.getElementById('serie').value;
        var factura = document.getElementById('factura').value;
        var nombre = "Factura";

        var cadena = nombre + " " + serie + " - " + factura;
        $('#observa_encabeza').val(cadena);

    }
</script>