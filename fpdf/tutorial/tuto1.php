<?php
require('../fpdf.php');

$fecha = '--Guatemala, 15 de mayo de 2018--';
$monto = '--Q.2,000.00--';
$letras = '--DOS MIL CON 00/100--';
$referencia = 'Factura A 101';
$beneficiario = '--MARTA MARIA IGLESIAS MENDIOLA--';
$nonegociable = '--NO NEGOCIABLE--';

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
$pdf->setxy(1,1);
$pdf->Cell(170,60,' ',0,1,l);
$pdf->setxy(26,25);
$pdf->Cell(95,5, $fecha,0,1,1);
$pdf->setxy(129,25);
$pdf->Cell(35,5, $monto,0,1,l);
$pdf->setxy(20,37);
$pdf->Cell(138,5, $letras,0,1,l);
$pdf->SetFont('Arial','',9);
$pdf->setxy(48,48);
$pdf->Cell(35,3, $referencia,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->setxy(20,31);
$pdf->Cell(105,5, $beneficiario,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,144,255);
$pdf->setxy(40,55);
$pdf->Cell(59,3, $nonegociable,0,1,C);

$pdf->Output();
?>
