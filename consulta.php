<?php
include('seguridad.php');
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

require_once('db/conexion.php');

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$usuario."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		ECHO $resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}

if(isset($_POST['contacto']))
    {
      $contacto = $_POST['contacto'];  
    }
    else{
      $contacto = '';
		}
		
		if(isset($_POST['moneda']))
    {
      $moneda = $_POST['moneda'];  
    }
    else{
      $moneda = '';
    }		

$sql = mysqli_query($conn, "SELECT DISTINCT A.ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES, APELLIDOS ,NOMBRE_EMPRESA))NOMBRES
														FROM tb_contacto A,
																tb_caso B,
															tb_acceso C
														WHERE A.ID_CONTACTO = B.ID_CONTACTO
															AND B.ID_CASO 		= C.ID_CASO
															AND C.ID_USUARIO 	= '".$usuario."'");


$detalle = mysqli_query($conn, "SELECT B.ID_CASO, B.DESCRIPCION, A.SALDO, A.MONEDA, B.CAUSA
								FROM tb_corriente A,
									 	 tb_caso B,
								     tb_contacto C
								WHERE A.ID_CASO 	= B.ID_CASO
								  AND B.ID_CONTACTO = C.ID_CONTACTO
									AND C.ID_CONTACTO = '".$contacto."'
									AND A.MONEDA 		= '".$moneda."'");
						
									
$saldo = mysqli_query($conn, "SELECT SUM(A.SALDO) DETALLE, A.MONEDA
															FROM tb_corriente A,
																		tb_caso B,
																	tb_contacto C
															WHERE A.ID_CASO 	= B.ID_CASO
																AND B.ID_CONTACTO = C.ID_CONTACTO
																AND C.ID_CONTACTO = '".$contacto."'
																AND A.MONEDA 		= '".$moneda."'");									
									
?>
  <script type="text/javascript">

    function selectItemByValue(elmnt, value){

    ///alert('elmnt: '+elmnt.name +' val: '+ value);

    for(var i=0; i < elmnt.options.length; i++)
      {
        if(elmnt.options[i].value == value)
          elmnt.selectedIndex = i;
      }
    }

  </script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

	<div class="wrapper-return">
		<button type="button" class="boton4"><a href="menu.php?id=22">Regresar</a></button>
	</div>

<div class="container">
	<div class="row">
		<form action="menu.php?id=9" method="post">
			  <div class="">
			        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
			            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
			            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>CONSULTAS CLIENTES</p></div>
			            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			        </div>     
			  </div>			
			  <div class="container">
			    <div class="row">

				<div class="col-md-12" style="margin-top: 25px;">
				  <div class="col-md-1"></div>
			      <div class="col-md-6">
			        <label>SELECCIONAR CLIENTE</label>
			        <select name="contacto" class="form-control upper" id="contacto" placeholder="SELECCIONAR CLILENTE">
			          <option value="">SELECCIONAR CLIENTE</option>
			            <?php
			            while ($rowx = mysqli_fetch_array($sql))
			            {
			              echo '<option value="' . $rowx['ID_CONTACTO']. '">' . $rowx['NOMBRES'] . '</option>' . "\n";
			            }
			            ?>           
			        </select>
					<script language="javascript">
					var numberMI = document.getElementById("contacto");
					selectItemByValue(numberMI,<?= "'".$contacto."'"?>);
					</script>
			      </div>
						<div class="col-md-3">
								<label>SELECCIONAR MONEDA</label>
								<select name="moneda" class="form-control" id="moneda">
									<option value="Q">QUETZALES</option>
									<option value="$">DOLARES</option>
								</select>
								<script language="javascript">
									var numberMI = document.getElementById("moneda");
									selectItemByValue(numberMI,<?= "'".$moneda."'"?>);
								</script>								
						</div>
			      <div class="col-md-1" style="margin-top: 35px;">
			            <div class="boton-formulario">
			              <button type="submit" class="boton3" style="margin-top: 18px;">BUSCAR</button>
			            </div>        
			      </div>					
				</div>

			    </div>
			  </div>

      <div class='bajar'>
        <button type="button" class="boton6"><a href="rep_general_saldo.php?tmp=<?php echo $contacto; ?>&view=<?php echo $moneda; ?>" target="_blank">Reporte PDF</a></button>   
      </div>				

    <div class="col-md-12 table-responsive" style="margin-top: 75px;">
      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
          <thead>
              <tr>
                  <th class="centrar">IR A DETALLE</th>
									<th class="centrar">CAUSA</th>
                  <th class="centrar">DESCRIPCI&Oacute;N DEL CASO</th>
                  <th class="centrar">SALDO</th>
              </tr>
          </thead>
          <tbody>
          <?php
      		while ($row = mysqli_fetch_array($detalle)){
        		
						$plata 	= number_format($row[2],2,'.',',');
						$description = utf8_encode(strtoupper($row[1]));

	          echo "<tr>";
						echo "<td><a href='menu.php?id=11&tmp=$row[0]'><img src='img/envia.png' width='25px;'></a></td>";
						echo "<td style='text-align: center; font-weight: bold;'>$row[4]</td>";
	          echo "<td style='text-align: center; font-weight: bold;'>$description</td>";
						echo "<td style='text-align: center; margin-left: 25px; font-weight: bold;'>$row[3]&nbsp;&nbsp;$plata</td>";
	        echo "</tr>";
	        } 
      	?>       
				<tr style='font-weight: bold; background-color: #ABB4BA;;'>
					<td>.</td>
					<td style='color: #000;'>SALDO A LA FECHA:</td>
					<td style='color: #000;'><?php echo date('d/m/Y'); ?></td>
					<td style='color: #000;'>
						<?php
						while($res = mysqli_fetch_array($saldo)){
							echo $res[1].'&nbsp;&nbsp;'.number_format($res[0],2,'.',',');
						}
						?>
					</td>
				</tr>
          </tbody>
      </table>
    </div>


  		</form>	
	</div>
</div>