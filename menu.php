<?php
session_start();
$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

require_once('db/conexion.php');

?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aboga</title>

        <!-- CSS -->        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400|Roboto:300,400,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/style_menu.css">

        <!-- Favicon and touch icons -->
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!--Librerias para Fechas-->
        <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <!--**********************-->

    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    
    <link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap.min.css">  

    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>      

    </head>

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="img/balance.png" style="width: 30px;" alt="">Aboga</a>
                <span id="mobUser"></span>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="menu.php?id=22">Dashboard</a></li>
                    <li><a href="menu.php?id=10">Carpetas</a></li>
                    <li><a href="menu.php?id=1">Contactos</a></li>
                    <li><a href="menu.php?id=2">Casos</a></li>
                    <li><a href="menu.php?id=16">Tareas</a></li>
                    <li><a href="menu.php?id=8">Calendario</a></li>
                    <!--li><a href="menu.php?id=9">Consultas</a></li>
                    <li><a href="menu.php?id=4">Cta.Corriente</a></li-->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Cta.Corriente <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="menu.php?id=4">Cuenta Corriente</a></li>
                            <li><a href="menu.php?id=9">Consultas</a></li>
                        </ul>
                    </li>                    
                   
                    <!--li><a href="menu.php?id=43">FACTURACI&Oacute;N</a></li>
                    <li><a href="menu.php?id=45">EDITAR FACTURACI&Oacute;N</a></li-->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Facturaci&oacute;n <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="menu.php?id=43">FACTURACI&Oacute;N</a></li>
                            <li><a href="menu.php?id=45">EDITAR FACTURACI&Oacute;N</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">CHEQUES <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="menu.php?id=47">PROVEEDORES</a></li>
                            <li><a href="menu.php?id=49">BANCOS</a></li>
                            <li><a href="menu.php?id=53">CUENTAS BANCARIAS</a></li>
                            <li><a href="menu.php?id=51">INGRESO FACTURAS PROVEEDORES</a></li>
                            <li><a href="menu.php?id=57">CONSULTA FACTURA PROVEEDORES</a></li>
                            <li><a href="menu.php?id=55">EMISI&Oacute;N CHEQUES</a></li>
                        </ul>
                    </li>                    
                    <li><a href="menu.php?id=41">BUSCAR DOCUMENTOS</a></li>
                </ul>


                <ul class="nav navbar-nav navbar-right">
                    <li class="helper"><a id="navUser" style='text-transform: uppercase; font-weight: bold;'><span style='font-weight: bold; text-transform: capitalize'>Usuario:   </span><?php echo $_SESSION['usuario']; ?></a></li>
                    <li><a href="menu.php?id=25">perfil</a></li>
            <?php

                $sql = mysqli_query($conn, "SELECT TIPO_USUARIO
                                            FROM tb_usuario
                                            WHERE ID_USUARIO = '".$usuario."'
                                            AND TIPO_USUARIO = 1");

                while($valida = $sql->fetch_array(MYSQLI_ASSOC)){

                $val_item = $valida['TIPO_USUARIO'];

                if($val_item == 1){
                    echo "<li><a href='menu.php?id=17'>admin</a></li>";      
                    }
                }              

            ?>
                    <li><a href="menu.php?id=40">salir <i class="fas fa-sign-out-alt"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <body onload="deshabilitaRetroceso();">
	<div class="wrapper-content-hv">
        <div class="wrapper-content-all">
            <div class="container">  
                        <?php

                        if (!isset($_REQUEST['id']) ){
                            require_once("dashboard.php");


                        }
                        else
                        {
                            $opc = $_REQUEST['id'];
                            if ( strlen($opc) >= 0 ) {
                                switch ($opc)
                                {  

                                    case 1  : require_once("contacto.php"); break;
                                    case 2  : require_once("casos.php"); break;
                                    case 3  : require_once("cuenta_corriente.php"); break;
                                    case 4  : require_once("cargo_abono.php"); break;
                                    case 5  : require_once("tareas.php"); break;
                                    case 6  : require_once("insert_cta_corriente.php"); break;
                                    case 7  : require_once("calendar.php"); break;
                                    case 8  : require_once("calendar_events.php"); break;
                                    case 9  : require_once("consulta.php"); break;
                                    case 10 : require_once("admin_archivos.php"); break;
                                    case 11 : require_once("detalle_saldo.php"); break;
                                    case 12 : require_once("crea_admin_area.php"); break;
                                    case 13 : require_once("borra_folder.php"); break;
                                    case 14 : require_once("sube_archivo.php"); break;
                                    case 15 : require_once("add_archivo.php"); break;
                                    case 16 : require_once("rep_tareas.php"); break;
                                    case 17 : require_once("adm_accesos.php"); break;
                                    case 18 : require_once("graba_accesos.php"); break;
                                    case 19 : require_once("edit_contacto.php"); break;
                                    case 20 : require_once("crea_sub_carpeta.php"); break;
                                    case 21 : require_once("crea_carpeta_nive_3.php"); break;
                                    case 22 : require_once("dashboard.php"); break;
                                    case 23 : require_once("sube_archivo2.php"); break;
                                    case 24 : require_once("envia_email.php"); break;
                                    case 25 : require_once("perfil.php"); break;
                                    case 26 : require_once("modifica_perfil.php"); break;
                                    case 27 : require_once("modifica_pass.php"); break;
                                    case 28 : require_once("graba_item.php"); break;
                                    case 29 : require_once("crea_usuario.php"); break;
                                    case 30 : require_once("inserta_caso.php"); break;
                                    case 31 : require_once("events_edit.php"); break;
                                    case 32 : require_once("elimina_usuario.php"); break;
                                    case 33 : require_once("edita_usuario.php"); break;
                                    case 34 : require_once("edit_caso.php"); break;
                                    case 35 : require_once("elimina_acceso.php"); break;
                                    case 36 : require_once("graba_tarea.php"); break;
                                    case 37 : require_once("elimina_acceso_casos.php"); break;
                                    case 38 : require_once("carpeta_niv_caso.php"); break;
                                    case 39 : require_once("mail_edit_tarea.php"); break;
                                    case 40 : require_once("logout.php"); break;
                                    case 41 : require_once("buscador.php"); break;
                                    case 42 : require_once("detalle_documentos.php"); break;
                                    case 43 : require_once("facturacion.php"); break;
                                    case 44 : require_once("inserta_factura.php"); break;
                                    case 45 : require_once("edit_factura.php"); break;
                                    case 46 : require_once("insert_edit_fac.php"); break;
                                    case 47 : require_once("proveedor.php"); break;
                                    case 48 : require_once("insert_proveedor.php"); break;
                                    case 49 : require_once("bancos.php"); break;
                                    case 50 : require_once("insert_bancos.php"); break; 
                                    case 51 : require_once("factura_proveedor.php"); break; 
                                    case 52 : require_once("inserta_fac_provee.php"); break;
                                    case 53 : require_once("cuentas.php"); break;
                                    case 54 : require_once("insert_cuentas.php"); break;
                                    case 55 : require_once("emision_cheque.php"); break;
                                    case 56 : require_once("insert_cheque.php"); break; 
                                    case 57 : require_once("rep_facturas_provee.php"); break;
                                    

                                    default : echo 'Esta opci&oacute;n no existe'; break;

                                }
                            }
                        }

                        ?>
            </div>
        </div>
        <!-- Javascript -->
        <?php
        if ($_REQUEST['id'] == 8 OR $_REQUEST['id'] == 16 ){
            
        }else{
            echo "<script src='menu/assets/js/jquery-1.11.1.min.js'></script>";
        }
     
        ?>


        <div class="push"></div>
    </div>

    </body>
    <!-- Footer -->
    <div class="wrapper-footer footer">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 footer-copyright">
                        &copy; Aboga - Derechos Reservados <?php echo date('Y'); ?>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <?php
    if($_REQUEST['id'] != 1){
        echo "<script src='menu/assets/bootstrap/js/bootstrap.min.js'></script>";
    }else{

    }

   
    ?>
    
    <script src="menu/assets/js/jquery.backstretch.js"></script>
    <script src="menu/assets/js/wow.min.js"></script>
    <script src="menu/assets/js/waypoints.min.js"></script>
    <script src="menu/assets/js/scripts.js"></script>

    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/jquery-ui.js"></script>

    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/funcion_select.js"></script>
    


    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
        $( function() {
            $( "#datepicker_1" ).datepicker();
        } );
        $( function() {
            $( "#datepicker_33" ).datepicker();
        } );
        $( function() {
            $( "#datepicker_34" ).datepicker();
        } );        
        $( function() {
            $( "#fecha1" ).datepicker();
        } );
        $( function() {
            $( "#fecha2" ).datepicker();
        } );
        $( function() {
            $( "#fec_notifica" ).datepicker();
        } ); 
    </script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Blfrtip',
                language:{'url': 'assets/js/spanish.json'},
                buttons: [

                ]

            } );
        } );
        $(document).ready(function() {
            $('#example1').DataTable( {
                dom: 'Blfrtip',
                language:{'url': 'assets/js/spanish.json'},
                buttons: [

                ]

            } );
        } );
        $(document).ready(function() {
            $('#exampleTable1').DataTable( {
                dom: 'Blfrtip',
                language:{'url': 'assets/js/spanish.json'},
                buttons: [

                ]

            } );
        } );        
    </script>
</html>
<script>
    function deshabilitaRetroceso(){
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button" //chrome
    window.onhashchange=function(){window.location.hash="no-back-button";}
    }
    


var anch = $(document).width();

if(anch < 992 ){
    $('#navUser').appendTo('#mobUser');
    $('.helper').remove();
}

</script>