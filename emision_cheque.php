<?php
require_once 'db/conexion.php';

$cuenta = mysqli_query($conn, "SELECT IDCUENTA, CUENTA, NUM_CUENTA
                                FROM tb_cuenta_banco
                                WHERE ESTATUS = 'A'");

$fac_provee = mysqli_query($conn, "SELECT ID_FACTURA, OBSERVACIONES, NOMBRE
                                    FROM tb_factura_proveedor");                                

?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: -1px !important;"><p>EMISI&Oacute;N CHEQUE</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="row">
    <form action="menu.php?id=56" method="post">
        <div class="col-md-12">
            <div class="col-md-6">
                <label for="">SELECCIONAR CUENTA</label>
                <select name="cuenta" id="" class="form-control">
                    <?php
                    while($row = mysqli_fetch_array($cuenta)){
                        echo '<option value="' . $row['IDCUENTA']. '">'. $row['CUENTA'] . ' - '.$row['NUM_CUENTA'].'</option>' . "\n";
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="">SELECCIONAR FACTURA / RECIBO</label>
                <select name="factura" id="" class="form-control">
                    <?php
                    while($res = mysqli_fetch_array($fac_provee)){
                        echo '<option value="' . $res['ID_FACTURA']. '">' . $res['NOMBRE'] . ' ** - ** '.$res['OBSERVACIONES'].'</option>' . "\n";
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-12">
            <div class="col-md-4">
                    <label for="">NUMERO DE CHEQUE</label>
                    <input type="text" name="numero_cheque" class="form-control upper center" placeholder="N&uacute;mero Cheque" required="">
            </div>
            <div class="col-md-4">
                    <label for="">MONTO DE CHEQUE</label>
                    <input type="text" name="monto" class="form-control upper center" placeholder="monto Cheque" required="">
            </div>
            <div class="col-md-4">
                    <label for="">FECHA DE PAGO</label>
                    <input type="text" name="fecha_pago" id="fecha" class="form-control upper center" placeholder="Fecha Pago" required="">
            </div>            
        </div>

        <div class="col-md-12">
            <div class="col-md-5">
                <label for="">INGRESAR REFERENCIA</label>
                <input type="text" name="referencia" class="form-control upper" placeholder="referencia">
            </div>
            <div class="col-md-5">
                <label for="">INGRESAR MOTIVO</label>
                <input type="text" name="motivo" class="form-control upper" placeholder="motivo">
            </div>
            <div class="col-md-2">
                <label for="">NEGOCIABLE</label>
                <select name="negociable" class="form-control">
                    <option value="S">SI</option>
                    <option value="N">NO</option>
                </select>
            </div>
        </div> 
    

    <div class="col-md-12 bajar">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="boton3">GRABAR</button>
        </div>
            <?php
            if(isset($_REQUEST['ftc']))
                {
                $var = $_REQUEST['ftc'];  
                echo "<div class='col-md-1'>";
                    echo "<a href='reporte_cheque.php?ftc=$var' target='_blanck'><button type='button' class='boton3'>IMPRESI&Oacute;N</button></a>";
                echo "</div>";
                }
                else{
                    $var = '0';    
                }

            ?>
    </div>              
    </form>
</div>

<script src="js/jquery.min.js"></script>

<script>
    $( function() {
        $( '#fecha' ).datepicker();
    } );
</script>