<?php
include('seguridad.php');
$nombre = $_SESSION['usuario'];
require_once 'db/conexion.php';
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>


<?php

$sql = mysqli_query($conn, "SELECT CONCAT(A.SERIE,'-',A.FACTURA)FAC, A.TOTAL, A.FECHA_EMISION, A.ESTATUS, A.TCHEQUE, A.TTC, A.TEFECTIVO, A.ID_FACTURA, A.FECHA_ANULA, B.ID_CASO, A.OBSERVACIONES
                                FROM tb_factura_cliente A,
                                    tb_detalle_factura B
                                WHERE A.ID_FACTURA = B.ID_FACTURA");


$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = '".$_REQUEST['id']."'");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}                               
?>

<style>
.mostrar {
	display: none;
}
</style>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: 2px !important;"><p>EDITAR FACTURAS</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

    <div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:170%;">
	        <thead>
	            <tr>
                    <th class="centrar ocultar">CASO</th>
	                <th class="centrar">FACTURA</th>
	                <th class="centrar">MONTO</th>
	                <th class="centrar">FECHA EMISI&Oacute;N</th>
					<th class="centrar">ESTADO</th>
                    <th class="centrar">FECHA ANULACI&Oacute;N</th>
                    <th class="centrar">TC</th>
                    <th class="centrar">TE</th>
                    <th class="centrar">TTC</th>
                    <th class="centrar">OBSERVACIONES</th>
                    <th class="centrar">MODIFICAR</th>
	            </tr>
	        </thead>
	        <tbody>
	       	<?php
               $var = 0;
			while ($row = mysqli_fetch_array($sql)){
                $var ++;

        echo "<script type='text/javascript'>

                function selectItemByValue(elmnt, value){

                for(var i=0; i < elmnt.options.length; i++)
                {
                    if(elmnt.options[i].value == value)
                    elmnt.selectedIndex = i;
                }
                }

                </script>";
                $caso       = $row[9];
                $fecha      = $row[2];
                $newDate    = date("d/m/Y", strtotime($fecha));
                $monto      = number_format($row[1],2,'.',',');
                $estatus    = $row[3];
                $tc         = $row[4];
                $ttc        = $row[5];
                $te         = $row[6];
                $fecha_anula = $row[8];
                $anulacion   = date("d/m/Y", strtotime($fecha_anula));
                $observaciones = $row[10];
                
                echo "<tr class='table-size'>";
                    echo "<form action='menu.php?id=46' method='post' onsubmit='return validacion()'>";

                        echo "<td class='ocultar'>$caso  <input typer='text' class='ocultar' name='caso' value='$caso'> </td>";
                        echo "<td >$row[0]  <input typer='text' class='ocultar' name='id_factura' value='$row[7]'> </td>";
                        echo "<td >$monto   <input typer='text' class='ocultar' id='monto' name='monto' value='$row[1]'> </td>";
                        echo "<td >$newDate <input typer='text' class='ocultar' name='fecha_emision' value='$newDate'> </td>";
                        echo "<td style='width: 150px;'>
                                <select name='estatus' id='estatus_$var' class='form-control'>
                                    <option value='C'>Credito</option>
                                    <option value='P'>Pagado</option>  
                                    <option value='A'>Anulado</option>
                                  </select>
                                </td>";

                        echo "<script language='javascript'>
                                var numberMI$var = document.getElementById('estatus_$var');
                                selectItemByValue(numberMI$var,'".$estatus."');
                            </script>";
                            echo "<td style='width: 80px;'><input class='form-control center mostrar' name='fecha_anula' id='date_$var' placeholder='Anulaci&oacute;n' value='$anulacion' readonly=''></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='tc$var' name='tc' placeholder='TC' readonly='' value='$tc'></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='te$var' name='te' placeholder='TE' readonly='' value='$te'></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='ttc$var' name='ttc' placeholder='TTC' readonly='' value='$ttc'></td>";
                        echo "<td style='width: 350px;'><input type='text' id='descri$var' name='observaciones' class='form-control' value='$observaciones'></td>";
                        echo "<td><button type='submit' id='boton' class='boton_add'>Modificar</button></td>";

                    echo "</form>";
                echo "</tr>";
                echo "<script src='menu/assets/js/jquery-1.11.1.min.js'></script>";
                echo "<script>
                        $( function() {
                            $( '#date_$var' ).datepicker();
                        } );
                    </script>";

                echo "<script>
                        $(document).ready(function(){

                            $('#estatus_$var').change(function(){

                                var estado$var = $('#estatus_$var').children('option:selected').val();
                                var fec_anula$var   = document.getElementById('date_$var').value;

                                if(estado$var == 'P'){
                                    $('#tc$var').attr('readonly',false);
                                    $('#te$var').attr('readonly',false);
                                    $('#ttc$var').attr('readonly',false);
                                }else{
                                    $('#tc$var').attr('readonly',true);
                                    $('#te$var').attr('readonly',true);
                                    $('#ttc$var').attr('readonly',true);
                                }

                                if(estado$var == 'A'){
                                    $('#date_$var').attr('readonly',false);
                                    $( '#date_$var' ).removeClass('mostrar');
                                }else{
                                    $('#date_$var').attr('readonly',true);
                                    $( '#date_$var' ).addClass('mostrar');
                                }
                                
                            }) 
                            

                        })
                      </script>";

                echo "<script>
                        function validacion(){
                            var estado$var = $('#estatus_$var').children('option:selected').val();

                            var tc          = document.getElementById('tc$var').value;
                            var te          = document.getElementById('te$var').value;
                            var ttc         = document.getElementById('ttc$var').value;
                            var monto       = document.getElementById('monto').value;
                            var descripcion = document.getElementById('descri$var').value;

                            
                            

                            suma = parseInt(tc)+parseInt(te)+parseInt(ttc);

                            
                            if(estado$var == 'P'){
                                if(suma != monto){
                                    alert('El Total no coincide con los montos...');
                                    return false;
                                }
                                if(suma == 0){
                                    alert('Debe de ingresar los metodos de pago....');
                                    return false;
                                }
                            }

                            if(estado$var == 'C'){
                                alert('No puede modificar el estado a Credito');
                                return false;
                            }

                            if(descripcion == ''){
                                alert('Debe ingresar la observación del movimiento...');
                                return false;
                            }


                        }
                      </script>";      
				} 
			?>           
	        </tbody>
	    </table>

    </div>