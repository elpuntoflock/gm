<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Proveedor Grabado con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=47"; 
    });
    }

</script>
<script>
function Error()
    {
      swal({title:"No se pudo grabar.... Ocurrio algun Error!", type:"danger", showConfirmButton:false, text:"VERIFICAR DATOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=47"; 
    });
    }

</script>
<script>
function Existe()
    {
      swal({title:"El Proveedor que quiere grabar ya Existe....", type:"warning", showConfirmButton:false, text:"VERIFICAR DATOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=47"; 
    });
    }

</script>	
	<title></title>
</head>
<body>
<?php
require_once 'db/conexion.php';

$empresa            = strtoupper($_POST['empresa']);
$nombre_provee      = strtoupper($_POST['nombre_proveedor']);
$apellido_provee    = strtoupper($_POST['apellido_proveedor']);
$direccion          = strtoupper($_POST['direccion']);
$zona               = $_POST['zona'];
$nit                = strtoupper($_POST['nit']);
$cui                = $_POST['cui'];

/******* DATOS CONTACTOS *******/
$n_contacto         = strtoupper($_POST['n_contacto']);
$ap_contacto        = strtoupper($_POST['ap_contacto']);
$cargo              = strtoupper($_POST['cargo']);
$telefono           = $_POST['telefono'];
$celular            = $_POST['celular'];
$tipo               = strtoupper($_POST['tipo']);


/********** CONTEO MAX + 1 DEL PROVEEDOR *******/

$conteo = mysqli_query($conn, "SELECT MAX(ID_PROVEEDOR)+1 MAXIMO
                                FROM tb_proveedor");

while($row = $conteo->fetch_array(MYSQLI_ASSOC)){
    $id_proveedor = $row['MAXIMO'];

    if($id_proveedor == 0){
    	$id_proveedor = 1;
    }else{
    	$id_proveedor = $row['MAXIMO'];
    }
        
}

$veri = mysqli_query($conn, "SELECT COUNT(*) CONTEO
                                FROM tb_proveedor
                                WHERE nit = '".$nit."'");

while($rxx = $veri->fetch_array(MYSQLI_ASSOC)){

    $veri_conteo = $rxx['CONTEO'];
        
}

if($veri_conteo == 0){

$insert = mysqli_query($conn, "INSERT INTO tb_proveedor (ID_PROVEEDOR,
                                                        NOMBRE_EMPRESA,
                                                        NOMBRE_PROVEEDOR,
                                                        APELLIDO_PROVEEDOR,
                                                        DIRECCION,
                                                        ZONA,
                                                        NIT,
                                                        CUI,
                                                        NOMBRE_CONTACTO,
                                                        APELLIDO_CONTACTO,
                                                        CARGO_CONTACTO,
                                                        TELEFONO_CONTACTO,
                                                        CELULAR_CONTACTO,
                                                        TIPO,
                                                        STATUS)values(
                                                        '".$id_proveedor."',
                                                        '".$empresa."',
                                                        '".$nombre_provee."',
                                                        '".$apellido_provee."',
                                                        '".$direccion."',
                                                        '".$zona."',
                                                        '".$nit."',
                                                        '".$cui."',
                                                        '".$n_contacto."',
                                                        '".$ap_contacto."',
                                                        '".$cargo."',
                                                        '".$telefono."',
                                                        '".$celular."',
                                                        '".$tipo."',
                                                        'A')"
                                                        );
                                                       
    
   if($insert == TRUE){
    echo "<script>Ingresado();</script>";
   }else{
    echo "<script>Error();</script>";
   }                                       

}else{
    echo "<script>Existe();</script>";
}

?>