function EliminaEvento(id){

	swal({   title: "Estás seguro?",   text: "No podrá recuperar esta información...!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Si, Eliminarlo..!",   cancelButtonText: "No, Cancelar por Favor..!",   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {    location.href='events_del.php?id='+id;   } else {     swal("Cancelado", "Tu información es segura :)", "error");   } });
	
}

function EliminaTipo(id){

	swal({   title: "Estás seguro?",   text: "No podrá recuperar esta información...!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Si, Eliminarlo..!",   cancelButtonText: "No, Cancelar por Favor..!",   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {    location.href='types_del.php?id='+id;   } else {     swal("Cancelado", "Tu información es segura :)", "error");   } });
	
}

function scrollNav() {
  $('.nav a').click(function(){  
    //Toggle Class
    $(".active").removeClass("active");      
    $(this).closest('li').addClass("active");
    var theClass = $(this).attr("class");
    $('.'+theClass).parent('li').addClass('active');
    //Animate
    $('html, body').stop().animate({
      scrollTop: $( $(this).attr('href') ).offset().top - 0
    }, 800);
    return false;
  });
  $('.scrollTop a').scrollTop();
}
scrollNav();




