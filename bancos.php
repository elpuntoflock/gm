<?php
require_once 'db/conexion.php';

$sql = mysqli_query($conn, "SELECT ID_BANCO, BANCO, FECHA_CREA, USUARIO_GRABA
                             FROM tb_banco");
?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
    <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
    <div class="col-md-4 titulo-seccion" style="margin-top: 5px !important;"><p>BANCOS</p></div>
    <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
</div>

<div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th># NUMERO</th>
                    <th>NOMBRE BANCO</th>
                    <th>FECHA CREACION</th>
                    <th>USUARIO GRABO</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while ($row = mysqli_fetch_array($sql)){
                    echo "<tr>";
                        echo "<td>";
                            echo $row['ID_BANCO'];
                        echo "</td>";
                        echo "<td>";
                            echo $row['BANCO'];
                        echo "</td>";     
                        echo "<td>";
                            echo $row['FECHA_CREA'];
                        echo "</td>";
                        echo "<td>";
                            echo $row['USUARIO_GRABA'];
                        echo "</td>";  
                    echo "</tr>";
                }
            ?>
            </tbody>
        </table>
</div>

	<div class=" bajar">
		<div class="row">
			<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
				<div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>NUEVOS INGRESOS</p></div>
				<div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			</div> 

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">NUEVO BANCO</button>
			</div>			
		</div>

<div id="ModalCrea" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
        <h4 class="modal-title">INGRESO DE PROVEEDORES</h4>
      </div>
      <div class="modal-body">
        <form action="menu.php?id=50" method="post">
            <div>
                <label for="">NOMBRE DE BANCO</label>
                <input type="text" name="banco" class="form-control upper" placeholder="NOMBRE DE BANCO">
            </div>
            <div class="boton-formulario bajar">
			    <button type="submit" class="boton3">GRABAR</button>
				<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
			</div>             
        </form>
      </div>

    </div>

  </div>
</div>         	
