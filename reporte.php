<?php
require_once('lib/pdf/mpdf.php');

require_once('db/conexion.php');

$usuario  = $_REQUEST['user'];
$caso     = $_REQUEST['caso'];
$fecha    = date('d/m/Y');

$datos = mysqli_query($conn, "SELECT a.CAUSA, a.ID_CONTACTO, CONCAT(b.NOMBRES,' ',b.APELLIDOS)NOMBRES
                                from tb_caso a,
                                    tb_contacto b
                                where a.id_contacto = b.id_contacto
                                  and a.id_caso = '".$caso."'");

while($resdat = $datos->fetch_array(MYSQLI_ASSOC)){

      $causa        = $resdat['CAUSA'];
      $contacto     = $resdat['ID_CONTACTO'];
      $nombre       = $resdat['NOMBRES'];
      $mon_inicial  = $resdat['MONTO'];
      $val_inicial  = number_format($mon_inicial,2,'.',',');
      $moneda       = $resdat['MONEDA'];

} 


$tareas = mysqli_query($conn, "SELECT A.ID, A.DESCRIPTION, A.START FECHA, A.END FECHA1
                                FROM events A,
                                  tb_acceso B
                                WHERE A.ID_CASO = B.ID_CASO
                                AND A.ID_CASO = '".$caso."'
                                AND B.ID_USUARIO = '".$usuario."'
                                ORDER BY FECHA ASC");

while ($result = mysqli_fetch_array($tareas)){

$loop = $loop .'
<tr>
<td style="text-align: left;">'.strtoupper($result[1]).'</td>
<td>'.$result[2].'</td>
<td>'.$result[3].'</td>
</tr>
';

}


$html = "<header class='clearfix'>
    <h1>REPORTE TAREAS POR CAUSA</h1>
    <br>
    <br>
    <div style='text-align: right;'>Fecha de Impresi&oacute;n: $fecha</div>
    <br>
    <br>
    <div id='logo'>
        <img src='img/logo/Law.png' style='width: 150px;'>
    </div>
    <br>
    <br>
    <br>
    <div>
    <ul style='font-weight: bold;'>
    <li>CAUSA:<span> $causa</span></li>
    <li>Cliente:<span>  $nombre</span></li>
    
  </ul>
    </div>

</header>
<main>
<!--Datos de Encabezado-->
<table>
<thead>
<tr style='background-color: #005691;'>
<th class='service' style='color: #fff;'>DESCRIPCI&Oacute;N DE TAREA</th>
<th class='desc' style='text-align: center; color: #fff;'>FECHA / HORA INICIO</th>
<th style='color: #fff;'>FECHA / HORA FINAL</th>

</tr>
</thead>
<tbody>
$loop;
</tbody>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<div style='text-align:center;'>Firma:___________________________________</div>
<div style='text-align:center;'>Lic. Victor P&eacute;rez</div>
<br>
<br>

</main>";  

$mpdf = new mPDF('c','A4');
$css = file_get_contents('lib/reportes/css/style.css');
$mpdf->writeHTML($css,1);
$mpdf->WriteHTML(utf8_encode($html));
//$mpdf->writeHTML($html);
$mpdf->Output('reporte.pdf','I');


?>
